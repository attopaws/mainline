! attoPAWS
! ================================================================
! v0.2.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!

#ifndef _SYSMESS_H_;
Message "[Mensajes por defecto del sistema PAWS]";
System_file;
Constant _SYSMESS_H_;

TablaMensajesSistema
with respuestas
    0  "Todo est� muy oscuro. No puedes ver nada.^"
    1  "Puedes ver "
    2  "^�Qu� hago ahora?^> "
    3  "^�Y ahora qu�?^> "
    4  "^�C�mo sigo?^> "
    5  "^�Qu� puedo hacer?^> "
    6  "Lo siento, no entiendo. Usa otras palabras.^"
    7  "No puedes ir por ah�.^"
    8  "No puedes hacer eso.^"
    9  "Llevas:^"
    10 " (puesto)"
    11 "Nada.^"
    12 "�Est�s seguro? (S/N) "
    13 "^�Quieres jugar otra vez? "
    14 "Gracias por jugar.^"
    15 "Vale.^"
    16 "[Pulsa una tecla] "
    17 "Has jugado "
    18 " movimiento"
    19 "s"
    20 ".^"
    21 "Has completado un "
    22 "% del juego.^"
    23 "No llevas eso puesto.^"
    24 "No puedes. Llevas puesto @02.^"
    25 "Ya tienes @03.^"
    26 "No veo eso por aqu�.^"
    27 "No puedes llevar m�s cosas.^"
    28 "No tienes eso.^"
    29 "Ya llevas puesto @02.^"
    30 "S"
    31 "N"
    32 "[M�s]"
    33 ""
    34 ""
    35 "^El tiempo pasa...^"
    36 "Has cogido @03.^"
    37 "Te pones @02.^"
    38 "Te quitas @02.^"
    39 "Has dejado @03.^"
    40 "No puedes ponerte @03.^"
    41 "No puedes quitarte @03.^"
    42 "No puedes quitarte @02 porque tienes las manos llenas.^"
    43 "Eso pesa demasiado.^"
    44 "Metes @02 en @06.^"
    45 "En @06 no hay @03.^"
    46 ", "
    47 " y "
    48 ".^"
    49 "No tienes @03.^"
    50 "No llevas puesto @03.^"
    51 ".^"
    52 "No hay @03 en @06.^"
    53 "nada.^"
    54 "Nombre del fichero> "
    60 "Salidas visibles: "
    61 "ninguna"
    62 ", "
    63 ".^"
    64 "No observas nada digno de menci�n en @02.^"
    65 "En @02 puedes ver "
    66 "No puedes mirar dentro de @03.^"
    67 "No puedes ver lo que hay dentro @04.^"
    68 "No hay nada dentro @04.^"
    69 "Oscuridad"  ! Titulo de una localidad a oscuras
    ;
#endif;

    
