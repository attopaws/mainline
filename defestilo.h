! attoPAWS
! ================================================================
! v0.2.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!

System_file;

Constant EstiloNormal_ 0;
Constant EstiloEnfatizado_ 1;
Constant EstiloPreFormateado_ 2;
Constant EstiloCabecera_ 3;
Constant EstiloSubcabecera_ 4;
Constant EstiloAlerta_ 5;
Constant EstiloNota_ 6;
Constant EstiloBloqueDeTexto_ 7;
Constant EstiloInput_ 8;
Constant EstiloUsuario1_ 9;
Constant EstiloUsuaio2_ 10;

Constant Indentacion_ 0;
Constant IndentacionParrafo_ 1;
Constant Justificacion_ 2;
Constant Tamanyo_ 3;
Constant Negrita_ 4;
Constant Cursiva_ 5;
Constant Proporcional_ 6;
Constant ColorTexto_ 7;
Constant ColorFondo_ 8;
Constant VideoInverso_ 9;
Constant Izquierda_ 0;
Constant Completa_ 1;
Constant Centrada_ 2;
Constant Derecha_ 3;

! Rutina para hacer mas legible la llamada a Glk
[ DefinirEstilo estilo que como;
    glk(176, 3, ! glk_stylehint_set(WinType_TextBuffer, ...
	estilo, que, como);
];

! Gancho para definir los estilos Glk antes de que se abra
! la ventana del juego, y as� disponer de estilos personalizados
! para el juego
_Gancho_ DefinirEstilos InicializarGlk
 with
    posicion GANCHO_EN_PRIMERA_POSICION,
    rutina [i;
	if (gg_main) return; ! Si la ventana ya est� abierta, no se
	                     ! pueden cambiar los estilos. Volvemos
	for (i=0:i<11:i++) DefinirEstilo(i, ColorFondo_, $000000);
	DefinirEstilo(EstiloNormal_, ColorTexto_, $a0a0a0);
	DefinirEstilo(EstiloAlerta_, ColorTexto_, $0050ff);
	DefinirEstilo(EstiloSubCabecera_, ColorTexto_, $A00000);
	DefinirEstilo(EstiloSubCabecera_, Tamanyo_, 1);
	DefinirEstilo(EstiloAlerta_, Tamanyo_, 0);
	DefinirEstilo(EstiloAlerta_, Negrita_, true);
	DefinirEstilo(EstiloAlerta_, Cursiva_, false);
	DefinirEstilo(EstiloInput_, Negrita_, true);
	DefinirEstilo(EstiloInput_, ColorTexto_, $00d0c0);
	DefinirEstilo(EstiloNota_, Justificacion_, Izquierda_);
	DefinirEstilo(EstiloNota_, ColorTexto_, $ffff00);
	DefinirEstilo(EstiloNota_, Cursiva_, false);
	DefinirEstilo(EstiloNota_, Negrita_, false);
	DefinirEstilo(EstiloNota_, Tamanyo_, -2);
	DefinirEstilo(EstiloBloqueDeTexto_, ColorTexto_, $ff0000);
	DefinirEstilo(EstiloBloqueDeTexto_, ColorFondo_, $400000);
	DefinirEstilo(EstiloBloqueDeTexto_, Justificacion_, Centrada_);
	DefinirEstilo(EstiloBloqueDeTexto_, Proporcional_, false);
    ];

    
