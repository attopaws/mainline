! attoPAWS
! ================================================================
! v0.2.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!

! Tabla de respuestas por defecto
!________________________________________________________________
#ifndef _RESPUESTAS_H_;
System_file;
Constant _RESPUESTAS_H_;
Message "[Tabla de respuestas por defecto attoPAWS]";

TablaRespuestas RespuestasDefecto   with respuestas
! Movimiento con el verbo IR
MIRAR      __       DESC
MIRAR      _        [; if (PREP==EN) return AUTOLISTCONT();
                       Verbo=EXAMINAR; rfalse; ]
EXAMINAR   __       EXITS     ! Comando X sin m�s
IR        __        "Debes especificar en qu� direcci�n ir."
IR        _         [x;
		      Verbo = Nombre1;
		      x = IntentarCaminar();
		      if (x==-1 or false) { return SYSMESS(7); }
	              if (x ~= 2) DESC();
		     ]
! Otros comandos tipicos
INVENTARIO _        INVEN
EXAMINAR INVENTARIO INVEN
EXAMINAR   __       AccionIncompleta
EXAMINAR   _	    AUTODESCRIBE
GRABAR     _	    SAVE
CARGAR     _	    LOAD
ESPERAR    _	    SYSMESS 35
AYUDA      __	    "Prueba en los foros de CAAD: http://www.caad.es"
REINICIAR  _	    RESTART
FIN        _	    [; if (QUIT()) {
		         TURNS();
		         END();
		       }
		    ]
!--------------
! Accion coger
!--------------
COGER    __       AccionIncompleta
COGER    TODO     DOALL 255
COGER    _        AUTOG
!-------------
! Accion Dejar
!-------------
DEJAR    __       AccionIncompleta
DEJAR    TODO     DOALL 254
DEJAR    _        AUTOD
!---------------
! Accion Ponerse/Quitarse
!---------------
PONER    __       AccionIncompleta
PONER    TODO     DOALL 255
PONER    _        AUTOW
QUITAR   TODO     DOALL 254
QUITAR   _        AUTOR
! ------------------
!  METER/SACAR
!-------------------
METER    __       AccionIncompleta
METER    TODO     DOALL 254
METER    _        [; if (WHATO()&&WHATO2()&&(Objeto2~=Objeto1))
                       return AUTOP(Objeto2); 
                  ]
SACAR    TODO     [; if (WHATO2()) DOALL(Objeto2);]
SACAR    __       AccionIncompleta
SACAR    _        [; if (WHATO2())
                      return AUTOT(Objeto2); 
                  ]
;

!================================================================
! Rutina que emite un mensaje cuando el jugador no ha escrito el
! comando completo
!================================================================
[ AccionIncompleta;
   mantener_verbo=true;
   "�Qu� quieres ", (vocabulario) verbo, "?";
];
#endif;




