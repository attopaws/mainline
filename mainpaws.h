! attoPAWS
! ================================================================
! v0.2.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!

! Punto de entrada principal para attoMOLE
! Aqu� se contiene la rutina main() que es ejecutada al arrancar el
! juego, y que toma el control del bucle principal del juego.
!
! Se implementa el meta-parser que separa el input en frases y se las
! da a analizar al parser-paws, as� como el mecanismo de ejecuci�n
! de la tabla de respuestas y de otras tablas del sistema.
!
! Se declaran todas las variables, propiedades y atributos que
! componen el modelo del mundo.

#ifndef _MAINPAWS_H_;
Include "parserpaws";
Message "[Bucle principal PAWS-like]";
System_file;
Constant _MAINPAWS_H_;

Constant MAX_ANIDAMIENTO_PROCESOS = 10;

! Arrays para leer la entrada, tokenizarla y parsearla
Array buff -> TAMANYO_BUFFER_INPUT;
Array par -> TAMANYO_BUFFER_PALABRAS;
Array par2 -> TAMANYO_BUFFER_PALABRAS;
Array indx -> TAMANYO_BUFFER_INDICES;
Array ignoradas --> 21;
Array result --> TAMANYO_BUFFER_PARSER;


!================================================================
! Propiedades globales
Property salidas;
Property titulo;
Property descripcion;
Property peso;
Property capacidad;
Property genero;
Property articulos;
Property nombre;
Property adjetivo;
Property additive decorado;


Constant G_MASCULINO 0;
Constant G_FEMENINO 1;
Constant G_PLURAL 2;

!================================================================
! Atributos para los objetos
Attribute prenda;
Attribute recipiente;
Attribute abierto;
Attribute especial;
Attribute luz;

!Attribute transparente;

!================================================================
! Variables, objetos y clases
!
! Variables globales donde dejar los resultados del parsing
Global Verbo;
Global Nombre1;
Global Nombre2;
Global Adj1;
Global Adj2;
Global Prep;
Global GramaticaActual; ! Apunta al objeto gram�tica que se usa
! actualmente. El programador pueda asignarle a esta variable un valor
! propio en la rutina Arranque. Si no lo hace, main() le asignar� el
! valor GramaticaEstandar.

! Variables globales equivalentes a algunos de los flag del paws.
Global PromptActual = 0;
Global Temporizacion_ = 0;
Global Turnos_ = 0;
Global Puntuacion_ = 0;

! Otras variables globales importantes para el mundo del juego
Global localizacion; ! Localidad actual
Global jugador;      ! Apunta al jugador
Global puestos;      ! Apunta al objeto que contiene las cosas que se
                     ! llevan puestas
Global limbo;        ! Apunta al objeto donde van a parar (o est�n)
                     ! los objetos que no est�n en el juego
Global Objeto1;      ! Apunta al objeto que podria ser NOMBRE1 ADJ1
Global Objeto2;      ! Apunta al objeto que podr�a ser NOMBRE2 ADJ2


! Variables para uso interno de PAWS
Global IndicePrimeraComilla;
Global IndiceUltimaComilla;
Global PosicionEnBufferPSI;
Global doall_ejecutandose_ = 0;
Global HuboTimeout_ = 0;
Global done_ = 0;        ! 1 = done, -1= notdone, 0 = ninguno
Global desc_token;   ! Utilizado para el mecanismo catch/throw
Global handle_save;  ! Handle del fichero para guardar/recuperar
Array  pila_procesos_ --> MAX_ANIDAMIENTO_PROCESOS;
                     ! Pila de llamadas a procesos
Global indice_pila_procesos_ = 0;

#Ifdef DEBUG;
Global depuracion_paws=0;   ! Para trazado

Constant BIT_TRAZAR_TABLAS 1;
Constant BIT_TRAZAR_ENTRADAS 2;
Constant BIT_TRAZAR_CONDACTOS 4;
Constant BIT_TRAZAR_PARSING 8;
#Endif; ! DEBUG

Object	    _  ! Marcador de "cualquiera" para la tabla de respuestas
	    ;
TNombre	__;    ! Marcador de "no se ha escrito m�s" para la tabla de
! respuestas que quiera saber cu�ndo se ha usado tan solo un verbo
! y as� poder distinguir entre MIRA y MIRA TAL COSA por ejemplo.


!________________________________________________________________
! Las clases para que el programador implemente objetos y
! localidades. Ambas derivan "temporalmente" de DummyClass1 y
! DummyClass2, pero esta herencia ser� sustituida en tiempo de
! inicializaci�n por una herencia de las clases TNombre y
! TAdjetivo. De este modo se permite que un objeto pueda poner en su
! lista "sinonimos" una secuencia de palabras de diccionario que ser�n
! reconocidas por el parser como NOMBRES, u otra en su secuencia de
! adjetivos que ser�n reconocidas como ADJETIVOS.
Class 	DummyClass1; !Horrible hack
Class 	DummyClass2; !Horrible hack

Class 	Objeto class DummyClass1 DummyClass2
 with 	numero, nombre, peso 1;
Class 	Localidad class DummyClass1 DummyClass2
 with 	numero, nombre
 has 	luz;   ! Todas las localidades tienen luz por defecto
Class 	Personaje class DummyClass1 DummyClass2
 with 	numero,
	nombre,
	peso 1,
	max_peso 200,
	capacidad 4;

! Clases para las diferentes tablas de respuestas
Class 	TablaRespuestas with respuestas;
Class   TablaMensajes with respuestas;
Class   TablaMensajesSistema class TablaMensajes;
Class   TablaMensajesUsuario class TablaMensajes;
Class   Proceso class TablaRespuestas;
Object 	TablasDeRespuestas;           ! Recipiente para las tablas de respuesta
Object  TablasDeMensajesDeSistema;   ! Recipiente para las tablas de sysmess
Object  TablasDeMensajesDeUsuario;   ! Recipiente para las tablas de mes


!================================================================
! Objetos predefinidos
!  (se usar�n si el programador del juego no proporciona otros �l
!  mismo y los asigna a las variables que se indican)
Personaje JugadorEstandar "ti" ! Asignado a Jugador
 with	sinonimos 'yo' 'mi' '-me' '-te';
Object 	Vestimenta;   ! Asignado a Puestos
Object  LimboEstandar;     ! Asignado a Limbo

!================================================================
! Ganchos creados en esta libreria

_Gancho_ ModificarEntrada;
! Es llamado despues de tokenizar, por si se quiere cambiar la
! entrada. Si retorna true, es que la ha cambiado y se retokeniza de
! nuevo.

_Gancho_ Proceso1;  ! Rutinas llamadas tras la descripcion
_Gancho_ Proceso2;  ! Rutinas llamadas antes del prompt, para manejo
                    ! de PSIs

_Gancho_ AntesDelDESC; ! LLamado justo antes de mostrar la descripci�n
_Gancho_ BuscarObjetoReferenciable;
            ! Tratar de asignar Nombre1 y Adjetivo1 con un objeto
            ! del juego


! El main, adem�s de inicializar el mecanismo de ganchos y todos los
! m�dulos, define dos strings din�micos para que llamen a las rutinas
! ImprimeObjeto1 e ImprimeObjeto2. De este modo el programador del
! juego puede hacer mensajes como: "No hay nada en @01.", lo cual viene
! a ser el equivalente del paws "No hay nada en _."
[ Main x;
    InicializarGanchos();
    EjecutarGancho(InicializarModulos);
    string 1 ImprimeEl_Objeto1;
    string 2 ImprimeElObjeto1;
    string 3 ImprimeUnObjeto1;
    string 4 ImprimeDelObjeto1;
    string 5 ImprimeEl_Objeto2;
    string 6 ImprimeElObjeto2;
    string 7 ImprimeUnObjeto2;
    string 8 ImprimeDelObjeto2;
    string 10 EstiloNormal;
    string 11 EstiloDebug;
    x="@11";  ! Para evitar errores de strings din�micos no usados
    if (gramaticaActual==0) GramaticaActual=GramaticaEstandar;
    if (jugador==0) jugador=JugadorEstandar;
    if (jugador.capacidad==0) jugador.capacidad=4;
    if (puestos==0) puestos=Vestimenta;
    if (limbo==0) limbo=LimboEstandar;
    Arranque();  ! Punto de entrada para que el jugador incorpore su
    ! rutina de bienvenida al juego y para que inicialice la
    ! localizacion, el objeto jugador, el objeto puestos y el objeto
    ! limbo

    ! Damos valor a las variables importantes que el programador no
    ! haya inicializado ya en el Arranque()
    if (localizacion==0) { ! Asignamos al primer objeto de la clase localidad
	objectloop(x ofclass Localidad) {
	    localizacion=x;
	    break;
	}
    }
    move jugador to localizacion;
    ! Movemos al limbo todos los objetos que no est�n en lugar alguno
    objectloop(x)
	if ((x ofclass Objeto)||(x ofclass Personaje))
	    if (parent(x)==0) move x to limbo;
    BuclePrincipal();
];

Global Mantener_verbo;

! Este es verdaderamente el bucle principal del juego, y que
! implementa el bucle del PAWS, en forma ligeramente ampliada para
! mayor flexibilidad
!
! Este bucle consta de las siguientes fases:
!  A) Descripci�n de la localidad
!    A.1) Imprime el campo "descripcion" de la localidad
!    A.2) Llama al Proceso1 para que el usuario a�ada m�s texto si
!         quiere
!  B) Ejecuci�n del Proceso2 (daemons)
!
!  C) Obtenci�n de una nueva l�nea de entrada si la anterior est� ya
!     completamente interpretada, o bien extracci�n de la siguiente frase
!     de la l�nea anterior, usando como delimitadores de frase la
!     lista de conjunciones declaradas en el juego (debe incluirse el
!     punto y la coma como conjunciones, pues este preparser no asume
!     que sea asi. En caso de haber obtenido una l�nea nueva, se pasa
!     por un gancho de filtrado para eliminarle los acentos y separar
!     los pronombres de los verbos.
!  D) Parseado de la frase mediante una llamada al parser-paws
!  E) Interpretaci�n de los resultados. Si falta el verbo se usa el
!     �ltimo verbo v�lido usado en la misma l�nea. Si hay pronombres
!     se sustituyen por el valor almacenado previamente en ellos
!  F) Ejecuci�n de la tabla de respuestas (que en realidad ejecuta
!     varias tablas de respuestas, en secuencia, deteniendose cuando una
!     de ellas retorna true, lo que permite implementar una "tabla de
!     respuestas por defecto" a ejecutarse en �ltimo lugar, y una
!     "espec�fica para el juego" que se ejecutar�a antes y cambiar�a
!     algunas de las respuestas est�ndar)
!  G) Intento de interpretar el comando como un verbo de movimiento
!     (si todas las tablas de respuesta anterior han fallado).
!
[ BuclePrincipal i j l quedan ultimo_v x npron apron pya;
    ultimo_v=0;  ! Ultimo verbo v�lido en esta l�nea
    npron=Ninguno; ! Qu� representa el pronombre (nombre)
    apron=Ninguno; ! Qu� representa el pronombre (adjetivo)
    DescribirLocalidad(Localizacion);
    EjecutarGancho(Proceso1);
    while(1)
    {
	! El comando catch de Glulx permite marcar un punto de la
	! ejecuci�n para poder retornar a �l desde cualquier otro
	! nivel de anidaci�n de rutinas. Esto es ideal para
	! implementar un DESC que vuelva siempre aqui sin importar
	! desde d�nde es ejecutado.
    	.catch_otro;
    	@catch desc_token ?principal;
	! Cuando @catch se ejecuta "Normalmente" (es decir, porque el
	! flujo de ejecuci�n llega a esta instrucci�n), entonces se
	! salta a la etiqueta "principal" y no se ejecuta nada de lo
	! que sigue. Si, en cambio, se llega al @catch debido a que se
	! ha hecho un "salto largo" (a trav�s del opcode @throw), es
	! decir, porque se ha ejecutado DESC, por ejemplo, entonces si
	! se ejecutar� lo que sigue. Adem�s, en este caso la variable
	! desc_token contiene un n�mero que se le pasa desde el opcode
	! @throw, lo cual nos sirve para identificar desde d�nde se ha
	! realizado el salto largo.
	doall_ejecutandose_=false;
	indice_pila_procesos_=0;
    	if (desc_token==2){
	    ! Hemos llegado desde un DESC
#Ifdef DEBUG;
	    if (depuracion_paws&BIT_TRAZAR_ENTRADAS)
		print "@11   [Que termina con DESC]@10^";
#Endif; ! DEBUG
	    EjecutarGancho(AntesDelDESC);
	    DescribirLocalidad(localizacion);
	    EjecutarGancho(Proceso1);
	    jump catch_otro;
	    ! Volvemos al @catch para marcar de nuevo el punto
    	}
	if (desc_token==3)
	{
	    ! Hemos llegado desde un NEWTEXT
#Ifdef DEBUG;
	    if (depuracion_paws&BIT_TRAZAR_ENTRADAS)
		print "@11   [Que termina con NEWTEXT]@10^";
#Endif; ! DEBUG
	    quedan=false;
	    jump catch_otro;
	    ! Volvemos al @catch para marcar de nuevo el punto
	}
	.principal;
	EjecutarGancho(Proceso2);
    	if (~~quedan)
	{
	    pya=false;
	    ! Obtener nueva l�nea de texto. Para ello emitimos el
	    ! prompt:
	    MostrarPrompt();
	    if (~~Mantener_verbo)
	    	ultimo_v=Ninguno; ! Borramos el verbo
	    Mantener_verbo=false;
	    glk(214, temporizacion_);
	    HuboTimeout_=false;
    	    BN_Input(gg_main, buff);  ! Leemos la l�nea y la convertimos
    	    Tokenizar(buff, par, indx); ! en palabras de diccionario
	    ! La l�nea siguiente llama al gancho ModificarEntrada,
	    ! pas�ndole como par�metros el buffer reci�n le�do, el
	    ! array de palabras una vez tokenizado y el array de
	    ! �ndices. Este gancho puede alterar el buffer de entrada
	    ! (de hecho lo hace, le quita los acentos y separa los
	    ! pronombres).

	    EjecutarGancho(ModificarEntrada, buff, par, indx);
	    l=1; ! l es el �ndice que indica la posici�n dentro del
	    ! array de palabras que toca ser parseada. Inicialmente la
	    ! ponemos a uno y seg�n vayamos parseando frases ir�
	    ! avanzando.
	}

	! Nos saltamos todas las conjunciones que puedan aparecer al
	! principio de la frase
	while (EsConjuncion(par-->l)) l++;
	!l--; ! Retrocedemos a la anterior palabra (la primera despu�s
	! de una conjunci�n

	! Buscar comillas, si las hay:
	IndicePrimeraComilla=0;
	IndiceUltimaComilla=0;
	for (i=l:i<par-->0:i++)
	    if (par-->i == '"//') {
		IndicePrimeraComilla=i;
		break;
		}
	for (i=IndicePrimeraComilla+1:i<par-->0+1:i++)
		if (par-->i == '"//')
	    {
		IndiceUltimaComilla=i;
		break;
	    }
	if ((IndiceUltimaComilla==0)&&(IndicePrimeraComilla~=0))
	    IndiceUltimaComilla=par-->0+1;

	! Desde esa palabra y hasta el final del buffer, miramos si
	! hay alguna conjunci�n, y vamos copiando a otro buffer
	! auxiliar (par2) todas las palabras no-conjunci�n,
	! sustituyendo todo el texto entrecomillado por la palabra
	! .frase (que al llevar un punto como letra, nunca podr� ser
	! escrita por el jugador)
	for (i=l,j=1:i<par-->0+1:i++)
	{
	    if (i==IndicePrimeraComilla)
	    {
		i=IndiceUltimaComilla+1;
		par2-->(j++)='.frase';
		if (i>=par-->0+1) break;
	    }
	    if (EsConjuncion(par-->i)) break;
	    par2-->(j++)=par-->i;
	}
	! Indicamos cu�ntas palabras tiene el buffer "par2"
	par2-->0=j-1;
	! Y actualizamos l para indicar d�nde comenzar� la siguiente
	! frase dentro del buffer "par"
	l=i;

	PosicionEnBufferPSI=IndicePrimeraComilla;

	! Si l==final del buffer "par" es que ya no quedan m�s frases
	! dentro del buffer. Ponemos a false la variable "quedan" para
	! que en la pr�xima interaci�n del bucle se pida una nueva
	! l�nea de texto. De no ser as�, la ponemos a true para que no
	! se pida.
	if (l>=par-->0+1) quedan=false;
	else quedan=true;

	! Indicamos al parser el tama�o del buffer "ignoradas" (que es
	! de 20). En este buffer retornar� los �ndices de las palabras
	! que han sido ignoradas durante el parsing. No lo usamos para
	! nada, pero en fin, me gustan los enfoques gen�ricos.
	ignoradas-->0=20;

	! Si par2 est� vac�o
	! Se llama al parser paws-like pasandole la frase en "par2",
	! la gram�tica que debe usar para el parsing, el array donde
	! ha de dejar los resultados y el array donde debe indicarnos
	! cu�ntas y cu�les de las palabras han sido ignoradas
#Ifdef DEBUG;
	if (depuracion_paws&BIT_TRAZAR_PARSING)
	    print "@11[Parser llamado con ", par2-->0, " palabras en el
		buffer]@10^";
#Endif; ! DEBUG
    	parserPAWS.parser(par2, GramaticaActual,
			 result, ignoradas);

#Ifdef DEBUG;
	if (depuracion_paws&BIT_TRAZAR_PARSING)
	{
	    print "@11[El parser ha ignorado ", ignoradas-->0, " palabras]^";
	    print "[Resultados del parsing:",
 		" V=", (name) result-->0,
		" N1=",	(name) result-->1,
		" A1=", (name) result-->2,
		" P=",  (name)  result-->3,
		" N2=", (name) result-->4,
		" A2=", (name) result-->5,
		" ]@10^";
	}
#Endif; ! DEBUG

	! Si no entiende ninguna palabra, mensaje 6
	if (ignoradas-->0 >= par2-->0)
	{
	    SYSMESS(6);
	    jump principal;
	}
	Turnos_++;
	! Actualizar verbo
	if (result-->0 ~= Ninguno) ultimo_v=result-->0;
	else result-->0 = ultimo_v;

	! Actualizar pronombres
	if (result-->4 == Pronombre) {
	    result-->4=npron;
	    result-->5=apron;
	}
	if (result-->1 ~= Pronombre or Ninguno) {
	    if (~~(pya || result-->1 has especial))
		!Siempre que no estuvieran ya actualizados,
		!esto es para quedarse solo con el primer objeto
		!cuando se enumeran varios, lo cual es el
		!comportamiento est�ndar de PAWS
	    {
		npron=result-->1;
		apron=result-->2;
		pya=true;
	    }
	}
	else if(result-->1 == Pronombre)
	{
	    result-->1=npron;
	    result-->2=apron;
	}

	x=false;  ! Indicador de que deben dejar de evaluarse las
	          ! tablas de respuesta
    	! Preparar variables globales
    	Verbo=result-->0;
    	Nombre1=result-->1;
    	Adj1=result-->2;
    	Prep=result-->3;
    	Nombre2=result-->4;
    	Adj2=result-->5;
	! Marcar si realmente se ha especificado un nombre
	if (par2-->0 < 3)
	{
	    Nombre2=__;
	    Adj2=__;
	    Prep=__;
	    Adj1=__;
	}
	if (par2-->0 <2)
	    Nombre1=__;

#Ifdef DEBUG;
	if (depuracion_paws&BIT_TRAZAR_PARSING)
	{
	    print "@11[Resultados del parsing reinterpretados:",
 		" V=",	(name) Verbo,
		" N1=", (name) Nombre1,
		" A1=", (name) Adj1,
		" P=",  (name) Prep,
		" N2=", (name) Nombre2,
		" A2=", (name) Adj2,
		" ]@10^";
	}
#Endif; ! DEBUG

	! Ejecutar las tablas de respuesta mientras retornen false
	objectloop(i in TablasDeRespuestas)
	{
	    done_=0;
	    x=EjecutarTablaRespuestas(Verbo, Nombre1, i);
	    if (x) break;
	    if (done_==1) {x=true; break;}
	    if (done_==-1) {
		SYSMESS(8);  !No puedes hacer eso
		x=true;
		break;
	    }
	}
	if (x) continue; ! Se ha salido con �xito. Volver al bucle
	                 ! principal

#Ifdef DEBUG;
	if (depuracion_paws&BIT_TRAZAR_ENTRADAS)
	    print "@11[Ninguna tabla encaja. Probando si es una
		salida]@10^";
#Endif; ! DEBUG

	! Fracaso. Ahora debe intentarse evaluar el comando como un
	! verbo de direcci�n. IntentarCaminar retornar� true si ha
	! conseguido mover al jugador con �xito. False si no le ha
	! movido, y -1 si ni siquiera era un verbo de direcci�n
	switch (IntentarCaminar())
	{
	  false:
#Ifdef DEBUG;
	    if (depuracion_paws&BIT_TRAZAR_ENTRADAS)
		print "@11 [Es una salida, pero no se puede ir]@10^";
#Endif; ! DEBUG
	    SYSMESS(7);  ! No puedes ir en esa direcci�n
	  -1: ! Ni siquiera es un verbo de direcci�n
#Ifdef DEBUG;
	    if (depuracion_paws&BIT_TRAZAR_ENTRADAS)
		print "@11 [No es verbo de direcci�n o error en salidas]@10^";
#Endif; ! DEBUG
	    SYSMESS(8);  ! No puedes hacer eso
	  2:
#Ifdef DEBUG;
	    if (depuracion_paws&BIT_TRAZAR_ENTRADAS)
		print "@11 [Salida inaccesible con mensaje personalizado]@10^";
#Endif; ! DEBUG
	    continue;    ! �xito, pero no mostrar localidad
	  true:
#Ifdef DEBUG;
	    if (depuracion_paws&BIT_TRAZAR_ENTRADAS)
		print "@11 [Salida ejecutada con �xito]@10^";
#Endif; ! DEBUG
	    DESC();      ! �xito, mostrar nueva localidad
	}
    }
];

[ Apilar_ x;
    if (indice_pila_procesos_ == MAX_ANIDAMIENTO_PROCESOS-1)
	"^[** Error PAWS: llamadas a procesos demasiado profundamente
	anidadas (MAX:", MAX_ANIDAMIENTO_PROCESOS, ") **]";
    pila_procesos_-->indice_pila_procesos_++=x;
];

[ Desapilar_;
    if (indice_pila_procesos_ == 0)
	"^[** Error PAWS: stack underflow. Avisa a Zak. **]";
    return pila_procesos_-->(--indice_pila_procesos_);
];


[ EjecutarTablaRespuestas  v n tabla
    i ver nom  accion p1 p2 p3 param x;

#Ifdef DEBUG;
    if (depuracion_paws & BIT_TRAZAR_TABLAS)
    {
    	print "@11[Ejecutando ";
	if (tabla ofclass Proceso) print "proceso";
	else if (tabla ofclass TablaRespuestas) print "tabla de
	    respuestas";
	print " ", (name) tabla, " con Verbo=", (name) v, ", Nombre1=",
	    (name) n, "]@10^";
    }
#Endif; ! DEBUG

    ! No se proporciona tabla v�lida
    if ((tabla==0)||(~~Tabla ofclass TablaRespuestas)) rfalse;

    ! Comprobar si la tabla tiene respuestas
    if (~~(Tabla provides respuestas)) rfalse;
    if (Tabla.#Respuestas/4==0) rfalse;

    ! Recorrer las entradas de la tabla
    i=0;
    while(i<Tabla.#respuestas/4)
    {
	ver=tabla.&respuestas-->i++;
	if ((~~(ver ofclass TVerbo)) && (ver~= _ or Ninguno))
	     "^[** Error PAWS: la tabla de respuestas ", (name)
		tabla, " no est� correctamente formada **]^";
	nom=tabla.&respuestas-->i++;
	if ((~~(nom ofclass TNombre)) && (nom ~= _) &&(nom~= Ninguno))
	     "^[** Error PAWS: tabla de respuestas ", (name)
		tabla,  " no est� correctamente formada **]^";
	accion=tabla.&respuestas-->i++;
	if ((MetaTipo(accion)~= TIPO_RUTINA or TIPO_STRING)
	    &&(~~(accion ofclass TablaRespuestas)))
	     "^[** Error PAWS: la tabla de respuestas ", (name)
		tabla,  " no est� correctamente formada en la
		entrada ", (name) ver, " ", (name) nom, " **]^";
	param=0;
	p1=tabla.&respuestas-->i++;
	if ((p1 ofclass TVerbo)||(p1==_)||(i>Tabla.#respuestas/4))
	    i--;
	else param++;
	p2=tabla.&respuestas-->i++;
	if ((p2 ofclass TVerbo)||(p1==_)||(i>Tabla.#respuestas/4))
	    i--;
	else param++;
	p3=tabla.&respuestas-->i++;
	if ((p3 ofclass TVerbo) ||(p1==_)||(i>Tabla.#respuestas/4))
	    i--;
	else param++;

	if ((ver==Verbo or _)||(V==_))
	    ! Y encaja el nombre
 	    if ((nom==Nombre1 or  _)||(N==_))
	    {
#Ifdef DEBUG;
		if (depuracion_paws&BIT_TRAZAR_ENTRADAS)
		    print "@11 [Entrada encajada: ", (name) ver, " ", (name)
		    	nom, " en la tabla ", (name) tabla, "]@10^";
#Endif; ! DEBUG
		switch (MetaTipo(accion))
		{
		 TIPO_STRING:
#Ifdef DEBUG;
		    if (depuracion_paws&BIT_TRAZAR_ENTRADAS)
			print "@11  [Que tiene una cadena]@10^";
#Endif; ! DEBUG
		    print (string) accion, "^";
		    DONE();
		 TIPO_OBJETO:
		    if (accion ofclass TablaRespuestas)
		    {
#Ifdef DEBUG;
			if (depuracion_paws&BIT_TRAZAR_ENTRADAS)
			    print "@11  [Que llama al proceso ", (name)
				accion, "]@10^";
#Endif; ! DEBUG
    			! Vamos a indicar que estamos procesando la
			! tabla actual
    			if (parent(tabla)) Apilar_(parent(tabla));
    			else Apilar_(tabla);
			x=EjecutarTablaRespuestas(verbo, nombre1, accion);
			Desapilar_();
#Ifdef DEBUG;
			if (depuracion_paws&BIT_TRAZAR_ENTRADAS)
			{
			    if ((x==true)||(done_==1))
				print "@11   [Que termina con DONE]@10^";
			    if ((x==-1)||(done_==-1))
				print "@11   [Que termina con NOTDONE]@10^";
			    if (x==0)
				print "@11   [Que no acepta la entrada]@10^";
			}
#Endif; ! DEBUG
		        if (x==true) DONE();
			if (x==-1) NOTDONE();
		    }
		 TIPO_RUTINA:
#Ifdef DEBUG;
		    if (depuracion_paws&BIT_TRAZAR_ENTRADAS)
			print "@11  [Que llama a una rutina o condacto]@10^";
#Endif; ! DEBUG
		    if (parent(tabla)) Apilar_(parent(tabla));
		    else Apilar_(tabla);
	    	    switch(param)
		    {
		     0: x=accion();
		     1: x=accion(p1);
		     2: x=accion(p1,p2);
		     3: x=accion(p1,p2,p3);
		    }
		    Desapilar_();
#Ifdef DEBUG;
		    if (depuracion_paws&BIT_TRAZAR_ENTRADAS)
		    {
			if ((x==true)||(done_==1))
			    print "@11   [Que termina con DONE]@10^";
			if ((x==-1)||(done_==-1))
			    print "@11   [Que termina con NOTDONE]@10^";
			if (x==0)
			    print "@11   [Que no acepta la entrada]@10^";
		    }
#Endif; ! DEBUG
		    if (x==true) DONE();
		    if (x==-1) NOTDONE();
		}
		if (done_) return(done_);
	    }
    }
    rfalse;
];

! Esta funci�n es similar a la anterior, pero no ejecuta una tabla de
! respuestas, sino una tabla de mensajes. En este tipo de tablas no se
! intenta encajar VERBO y NOMBRE, sino tan solo un n�mero que se
! recibe como par�metro.
[ EjecutarTablaMensajes  tabla n
    i accion;

    ! No se proporciona tabla v�lida
    if ((tabla==0)||(~~Tabla ofclass TablaMensajes)) rfalse;

    ! Comprobar si la tabla tiene respuestas
    if (~~(Tabla provides respuestas)) rfalse;
    if (Tabla.#Respuestas/4==0) rfalse;

    ! Verificar que el n�mero de words en la tabla sea m�ltiplo de 2,
    ! ya que cada entrada de la tabla de respuestas ocupa 2 words
    if ((Tabla.#respuestas/4)%2)
	"[** PAWS: Error de programaci�n. La
    	tabla de mensajes ", (name) tabla, " parece incompleta **]^";

    ! Recorrer las entradas de la tabla
    for (i=0:i<(Tabla.#respuestas/4):i=i+2)
    {
	! Si encaja el numero
	if (n==Tabla.&respuestas-->i)
	{
	    accion=Tabla.&respuestas-->(i+1);
	    ! Y ejecutarla (segun su tipo)
	    switch (MetaTipo(accion))
	    {
	     TIPO_STRING: print (string) accion;
	    	rtrue;
	     TIPO_RUTINA: return accion();
	     default:
	    	"[** PAWS: Error de programaci�n. La tabla de
	    	respuestas ", (name) tabla,
	    	" tiene formato incorrecto en la l�nea ", n, " **]^";
	    }
	}
    }
    rfalse;
];



!****************************************************************
!****************************************************************
!****************************************************************
!****************************************************************
!                  IMPLEMENTACI�N DE GANCHOS
!****************************************************************
!****************************************************************
!****************************************************************
!****************************************************************
! Manejo del TIMEOUT. Observar que por defecto la temporizaci�n est�
! desactivada. Se activa tan pronto como el usuario inicialice la
! variable Temporizacion con un valor distinto de cero (n�mero de
! milisegundos a esperar antes del timeout)
_Gancho_ GlkTimeout EventoGLk
    with rutina [evento;
	     if (evento-->0 == 1)
	     {
		 ! Ha ocurrido un timeout. Mostrar mensaje "El tiempo
		 ! pasa, ejecutar proceso 2 y reponer el prompt
		 HuboTimeout_=true;
		 glk($86,0); ! Estilo normal
		 SYSMESS(35);  ! El tiempo pasa...
		 EjecutarGancho(Proceso2);
       		 MostrarPrompt();
		 glk($86,8);  ! Estilo input
		 return;
	     }
	 ];

! Este gancho inicializa un mont�n de objetos, movi�ndolos a sus
! lugares apropiados. Tambi�n a�ade a las categor�as TNombre y
! TAdjetivo las palabras que aparecen en el campo "sinonimos" y
! "adjetivo" de los objetos de la clase Objeto, a la vez que los
! convierte en objetos de la clase TNombre y TAdjetivo mediante un
! horrible hack.
_Gancho_ InicializarPaws InicializarModulos
 with 	rutina [ i j k p npal;
	    ! Movemos todos los objetos tipo Tabla  a su recipiente
	    ! adecuado.
	    objectloop(i)
	    {
		if ((i ofclass TablaRespuestas)&&(~~(i ofclass Proceso)))
		    move i to TablasDeRespuestas;
		if (i ofclass TablaMensajesSistema)
		    move i to TablasDeMensajesDeSistema;
		if (i ofclass TablaMensajesUsuario)
		    move i to TablasDeMensajesDeusuario;
	    }
	    ! Aumentamos el vocabulario del juego con los nombres y
	    ! adjetivos definidos en los propios objetos y
	    ! localidades.
	    npal = InicializarVocabulario.sig_id;
	    objectloop(i)
	    {
		if ((i ofclass Objeto)||(i ofclass Localidad)
		    ||(i ofclass Personaje))	{
		    if (i provides sinonimos)
		     	for (j=0:j<i.#sinonimos/WORDSIZE:j++) {
		     	    p=i.&sinonimos-->j;
			    if (p->0 == $60)  !Es palabra de diccionario
			    {
				if (i.numero == 0)
				    i.numero = npal++;
				if (p->(DICT_WORD_SIZE + 3) +
				    p->(DICT_WORD_SIZE + 4) ~= 0)
				    print "^[** AVISO: la palabra '",
					(address) p, "' ya estaba definida en
					el vocabulario, y se ha redefinido en ",
					(name) i, " **]^";
		     	    	p->(DICT_WORD_SIZE + 3) = i.numero / 256;
		     	    	p->(DICT_WORD_SIZE + 4) = i.numero % 256;
		     	    	p->#dict_par1=p->#dict_par1 |
				    BIT_ES_NOMBRE;

				!Horrible hack. Cambiamos la clase de
				!la que deriva el objeto en tiempo de
				!ejecuci�n!!
				p=i.&2;
				for (k=0:k<i.#2/4:k++) if (p-->k==DummyClass1)
				    p-->k=TNombre;
		     	    }
			}
		    if (i provides adjetivo)
		     	for (j=0:j<i.#adjetivo/WORDSIZE:j++) 	{
		     	    p=i.&adjetivo-->j;
			    if (p->0 == $60)   {
				if (i.numero == 0)
		     		    i.numero = npal++;
				if (p->(DICT_WORD_SIZE + 3) +
				    p->(DICT_WORD_SIZE + 4) ~= 0)
				    print "^[** AVISO: la palabra '",
					(address) p, "' ya estaba definida en
					el vocabulario, y se ha redefinido en ",
					(name) i, " **]^";
		     	    	p->(DICT_WORD_SIZE + 3) = i.numero / 256;
		     	    	p->(DICT_WORD_SIZE + 4) = i.numero % 256;
		     	    	p->#dict_par1 = p->#dict_par1 |
				    BIT_ES_ADJETIVO;

				!Horrible hack. Cambiamos la clase de
				!la que deriva el objeto en tiempo de
				!ejecuci�n!!
				p=i.&2;
				for (k=0:k<i.#2/4:k++) if (p-->k==DummyClass2)
				    p-->k=TAdjetivo;
			    }
			}
		}
	    }

	];


! Este gancho quita los acentos de las palabras que no ha comprendido,
! e intenta separar el pronombre final -lo en las palabras que no ha
! comprendido, siempre que el resultado sea un verbo comprensible
! Para ello se apoya de otras rutinas programadas m�s abajo en este
! mismo fichero.
_Gancho_ Transformar ModificarEntrada
    with rutina [ buff par indx
    i modificado donde longitud x;
	     for (i=1:i<par-->0+1:i++)
	     {
		 if (par-->i==0)  !Si la palabra no ha sido comprendida
		 {
		     ! Averiguamos donde est� en el buffer de letras
		     donde=IndiceDePalabraEnBuffer(indx,i);
		     ! Y cu�ntas letras la forman
		     longitud=LongitudDePalabraEnBuffer(indx,i);
		     ! Le quitamos los acentos
		     if (QuitarAcentos(buff, donde, longitud))
			 Tokenizar(buff,par, indx);

		     ! Miramos cu�ntas letras del final podr�an ser un
		     ! pronombre (p. ej: si la palabra termina en
		     ! "LO", x valdr� 2, si termina en "LOS" valdr� 3)
		     x=TerminaEnPronombre(buff+donde,longitud);
		     if (x && x<longitud)
			 ! Si efectivamente la palabra termina en
			 ! pronombre probaremos a separarlo
		     {
			 ! Comprobamos si lo que queda a la izquierda
			 ! del pronombre es un verbo
			 if (VerificarSiEsVerbo(buff+donde,
						longitud-x))
			 {
			     ! Si lo es, insertamos " -" en el punto
			     ! apropiado, con lo que COGELO se
			     ! convierte en COGE -LO
			     InsertarEnBuffer(buff, donde+longitud-x,
					      " -");
			     ! Puesto que hemos cambiado el buffer de
			     ! letras, hay que retokenizar
			     Tokenizar(buff, par, indx);
			     modificado=false;
			     ! Ya hemos tokenizado nosotros, no es
			     ! necesario tokenizar una vez m�s desde
			     ! fuera.
			 }
		     }
	     	 }
	     }
	     return modificado;
	     ! Indicamos a quien ha llamado al gancho si hemos
	     ! modificado el buffer y es necesaria una tokenizaci�n
	     ! m�s.
	 ];

! Este gancho es llamado cuando una partida es restaurada. Hay que
! reinicializar las variables glk (gg_main y handle_save son las
! �nicas utilizadas en este caso)
_Gancho_ RestaurarGG ReinicializarGlk
    with rutina [ id;
	     gg_main=0;
	     handle_save=0;
	     id = glk($0040, 0, gg_arguments); ! stream_iterate
    	     while (id) {
        	 switch (gg_arguments-->0) {
            	  301: handle_save = id;
        	 }
        	 id = glk($0040, id, gg_arguments); ! stream_iterate
    	     }
	     id = glk($0020, 0, gg_arguments); ! window_iterate
    	     while (id) {
        	 switch (gg_arguments-->0) {
            	  201: gg_main = id;
        	 }
        	 id = glk($0020, id, gg_arguments); ! window_iterate
    	     }
	 ];


! Quita los acentos de la palabra que se le ordena
! Recibe 3 par�metros:
!    buf   Buffer de letras
!  donde   �ndice donde comienza la palabra a cambiar
!      l   N�mero de letras de la palabra a cambiar
[ QuitarAcentos buf donde l
    c modif i;
    for (i=donde:i<donde+l:i++)
    {
	c=0;
	switch(buf->i)
	{
	 '�','�': c='a';
	 '�','�': c='e';
	 '�','�': c='i';
	 '�','�': c='o';
	 '�','�': c='u';
	 '�','�': c='u';
	 '�','�': c='n';
	}
	if (c) {buf->i=c;
	    modif=true;
	}
    }
    return modif;
];

! Sufijos reconocibles como pronombres
Array arraypronombres table "lo" "la" "los" "las" "le" "les" "me" "te";

! Rutina que comprueba si una palabra dada termina en uno de los
! anteriores, y en ese caso retorna cu�ntas letras tiene el sufijo con
! el que ha casado
! Par�metros:
!    buf   Direcci�n de memoria donde est� la primera letra de la
!          palabra a considerar
!    len   N�mero de letras de esa palabra.

[ TerminaEnPronombre buf len i l;
    for (i=1:i<=arraypronombres-->0:i++)
    {
	l=ImprimirEnMemoria(gg_arguments, 32, arraypronombres-->i);
	if (TerminaEn(buf, len, gg_arguments, l))
	    return l;
    }
    rfalse;
];

[ TerminaEn b1 l1 b2 l2 a b;
    if (l2>=l1)
	rfalse;
    while (l2>0)
    {
	l1--;
	l2--;
	a=glk($A0, b1->l1); ! Pasar a min�scula
	b=glk($A0, b2->l2); ! la letra de cada buffer
	if (a~=b) rfalse;
    }
    rtrue;
];

! Rutina que inserta una cadena dada en una posici�n de un buffer.
! Parametros:
!   buf   Buffer de caracteres (se espera en buff-->0 su longitud
!   pos   Offset con respecto a buff donde se prentende insertar
!         (offset=4 es la primera letra del buffer)
!   str   Cadena a insertar (tipo string)
!
[ InsertarEnBuffer buf pos str i l;
    l=ImprimirEnMemoria(gg_arguments, 32, str);
    for (i=buf-->0+WORDSIZE-1:i>=pos:i--)
    {
	buf->(i+l)=buf->i;
    }
    buf-->0=buf-->0+l;
    for (i=pos:i<pos+l:i++)
	buf->i=gg_arguments->(i-pos);
];

! Dado un buffer de tipo indx como el retornado por Tokenizar, esta
! rutina extrae de �l la informaci�n sobre el �ndice de la palabra
! i�sima,
[ IndiceDePalabraEnBuffer b i;
    return (b-->(i*2-1));
];

! Dado un buffer de tipo indx como el retornado por Tokenizar, esta
! rutina extrae de �l la informaci�n sobre la longitud de la palabra
! i�sima,
[ LongitudDePalabraEnBuffer b i;
    return (b-->(i*2));
];

! Recibe en "arr" la direcci�n de memoria donde comienza una cadena
! de "len" letras. Retorna true si esa palabra est� en el diccionario
! marcada como verbo, y false si no.
[ VerificarSiEsVerbo arr len
    dictlen entrylen res i val;

    dictlen = #dictionary_table-->0;
    entrylen = DICT_WORD_SIZE + 7;
    if (len > DICT_WORD_SIZE)
	len = DICT_WORD_SIZE;
    for (i=0 : i < len : i++)
	gg_tokenbuf->i = glk($00A0, arr->i); ! char_to_lower
    for ( : i < DICT_WORD_SIZE : i++) {
	gg_tokenbuf->i = 0;
    }
    val = #dictionary_table + WORDSIZE;
    @binarysearch gg_tokenbuf DICT_WORD_SIZE val entrylen dictlen
	1 1 res;
    if (res->#dict_par1&BIT_ES_VERBO) rtrue;
    rfalse;
];

!================================================================
! Algunas rutinas m�s
[ MostrarPrompt m;
    if (PromptActual==0)
    {
	switch(random(10))
	{
	 0,1,2: m=2;
	 3,4,5: m=3;
	 6,7,8: m=4;
	 default: m=5;
	}
    }
    else m=PromptActual;
    SYSMESS(m);
];

! DescribirLocalidad
[ DescribirLocalidad loc;
    if (ISLIGHT())
    {
    	if (loc provides titulo)
    	{
	    glk($86,4);
	    new_line;
	    Output(loc, titulo);
	    new_line;
	    glk($86,0);
    	}
    	if (loc provides descripcion) Output(loc, descripcion);
    	else "^[** Error PAWS: localidad ", (name) loc, " no tiene
	    descripci�n **]";
    } else {
	glk($86,4); new_line;
	SYSMESS(69); new_line;  ! "Oscuridad"
	glk($86,0);
	SYSMESS(0);  ! Todo est� muy oscuro
    }
];



! Esta rutina mira si el verbo es uno de los verbos de direcci�n, e
! intenta mover al jugador en esa direcci�n si es as�
[ IntentarCaminar  lugar;
    if (Verbo == Ninguno)
        return -1;

    lugar = Verbo;
    while (lugar ofclass TVerbo)
    {
	lugar = IntentarIr (lugar, localizacion);

	if (metaclass (lugar) == Routine)
	    lugar = lugar();
    }

    switch (metaclass (lugar))
    {
	Object:
	    if (~~(lugar ofclass Localidad))
	    {
		! if (~~(lugar provides rutina))
		! {
		    print "[** Error PAWS: Una salida de la localidad ",
			(name) localizacion, " lleva a un objeto (",
			(name) lugar, ") que no es una localidad **]^";
		    return -1;
		! }
		! lugar = lugar.rutina();
		! if (lugar) return 2;
		! if (~~(lugar ofclass Localidad)) rfalse;
	    }
	    GOTO (lugar);
	    rtrue;
	String:
	    print (string) lugar, "^";
	    return 2;
	nothing:
	    if (lugar < 0)  ! N� msg usuario a imprimir cambiado de signo
	    {
		MES (-lugar);
		return 2;
	    }
    }

    ! Dos casos posibles llegan aqu�: si lugar vale false (normal) o si vale
    ! alguna otra cosa (error)
    if (lugar ~= false)
	return -1;
    rfalse;
];

! Esta rutina averigua si en la direcci�n especificada por VERBO hay
! una salida, y si es as�, retorna el objeto-string-rutina asociada a
! esa salida.
[ IntentarIr n loc  dir i;
    ! Tomamos la primera palabra del objeto que se nos pasa como
    ! direcci�n. Observar que en principio el objeto puede ser de la
    ! clase TNombre, TVerbo o cualquier otra
    if (~~(n provides sinonimos))
	rtrue;
    ! Y miramos si esa primera palabra es un verbo
    dir = EsVerbo (n.sinonimos);
    ! Si no es verbo o no es verbo de direcci�n, no podremos `ir'
    if ((dir == 0) || (dir hasnt especial))
	rtrue;

    ! Ahora hay que mirar las salidas de la localidad, a ver si
    ! alguna coincide con ese verbo.
    if (loc provides salidas)
	for (i = 0: i < (loc.#salidas / WORDSIZE): i = i + 3)
	    if (loc.&salidas-->i == dir)  ! �Coincide?
		return loc.&salidas-->(i + 2);  ! Devolvamos el obj-str-rut
    rfalse;
];


! Rutina para marcar verbos como de direcci�n
[ SonVerbosDeDireccion _vararg_count v i;
    for (i=0:i<_vararg_count:i++)
    {
	@copy sp v;
	if (~~(v ofclass TVerbo))
	    "^[** Error de PAWS: SonVerbosDeDireccion ha sido llamado
	    con el par�metro ", (name) v, " que no es un verbo **]";
	give v especial;
    }
];


!================================================================
! RUtinas de utilidad para imprimir
!
! Imprime el nombre corto de un objeto, o su nombre interno si no
! tiene nombre corto. Pero por defecto, los nombres internos son
! asignados por el compilador como una cadena encerrada entre
! par�ntesis. Por tanto algo como print (nombre) Verbo; generar�a en
! la salida "(COGER)", si el nombre interno del verbo era COGER. En
! esta rutina se pasa a minusculas ese nombre interno y se eliminan
! los par�ntesis de alrededor si los hubiera
Array auxiliar -> 84; ! M�ximo de 80 letras en el nombre interno. Ya
                      ! est� bien :-)
[ Vocabulario p i;
    auxiliar-->0=ImprimirEnMemoria(auxiliar+4, 80, p);
    for (i=4:i<auxiliar-->0+4:i++)
	if (auxiliar->i ~= '(' or ')')
	    print (char) glk($A0, auxiliar->i);
];

! Rutinas para imprimir art�culos apropiados
[ El_ x;
    if (x provides articulos) {
	Output(x.&articulos-->0);
	if (x.&articulos-->0) print " ";
    }
    else if (x provides genero)
       	switch(x.genero)
       	{
       	 0: print "El ";
     	 1: print "La ";
     	 2: print "Los ";
     	 3: print "Las ";
    	}
    else print "El ";
    print (name) x;
];

[ el x;
    if (x provides articulos) {
	Output(x.&articulos-->1);
	if (x.&articulos-->1) print " ";
    }
    else if (x provides genero)
    	switch(x.genero)
    	{
     	 0: print "el ";
     	 1: print "la ";
     	 2: print "los ";
     	 3: print "las ";
    	}
    else print "el ";
    print (name) x;
];

[ un x;
    if (x provides articulos) {
	Output(x.&articulos-->2);
	if (x.&articulos-->2) print " ";
    }
    else
    switch(x.genero)
    {
     0: print "un ";
     1: print "una ";
     2: print "unos ";
     3: print "unas ";
    }
    print (name) x;
];

[ del x;
    ImprimirEnMemoria(gg_arguments, 16, el, x);
    if (gg_arguments->0 == 'e') print "d";
    else print "de ";
    print (el) x;
];

[ al x;
    print "a";
    ImprimirEnMemoria(gg_arguments, 16, el, x);
    if (gg_arguments->0 == 'e') { print "l ", (name) x; return;
    }
    else print " ";
    print (el) x;
];

[ o x;
    if (x provides genero)
	switch (x.genero)
	{
	 0: print "o";
	 1: print "a";
	 2: print "os";
	 3: print "as";
	}
    else print "o";
];

[ ImprimeEl_Objeto1;
    print (El_) Objeto1;
];

[ ImprimeEl_Objeto2;
    print (El_) Objeto2;
];

[ ImprimeElObjeto1;
    print (el) Objeto1;
];

[ ImprimeElObjeto2;
    print (el) Objeto2;
];
[ ImprimeUnObjeto1;
    print (un) Objeto1;
];

[ ImprimeUnObjeto2;
    print (un) Objeto2;
];
[ ImprimeDelObjeto1;
    print (del) Objeto1;
];

[ ImprimeDelObjeto2;
    print (del) Objeto2;
];

[ EstiloDebug;
    glk($86, 6);  ! Estilo Nota
];

[ EstiloNormal;
    glk($86, 0);
];
#endif;
