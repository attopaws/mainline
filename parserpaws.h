! attoPAWS
! ================================================================
! v0.2.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!

! Parser tipo PAWS
!
! Este parser toma como entrada un array de palabras y trata de
! encajarlas con una gram�tica que recibe como segundo par�metro. La
! gram�tica es un grafo mediante el cual se especifica un
! aut�mata. Parte de un estado inicial y cada nodo del grafo
! especifica un test que aplicar sobre la palabra actual, y el nuevo
! nodo al que llega en caso de �xito. Cada vez que el aut�mata cambia
! de nodo, ejecuta la "rutina" asociada a ese nodo pasandole la
! palabra actual.
!
! Esto permite implementar una gram�tica paws-like totalmente flexible
! que admita cualquier token en cualquier orden, o algo un poco m�s
! estricto que espere una ordenaci�n del tipo VERBO NOMBRE ADJETIVO
! En ambos casos el parser ser�a el mismo y s�lo cambia la gram�tica
! asociada a �l.
!
! Se proporcionan aqu� tambi�n funciones de an�lisis l�xico que
! determinan si una palabra dada es nombre o verbo o adjetivo (puede
! ser las tres cosas).
!
! El resultado del parsing queda en un array "result" cuya estructura
! depende de c�mo la maneje el grafo gramatical, pero que en la
! implementaci�n por defecto que aqu� se da es la siguiente:
!
!  result-->0 = verbo detectado
!  result-->1 = nombre1
!  result-->2 = adjetivo1
!  result-->3 = preposicion
!  result-->4 = nombre2
!  result-->5 = adjetivo2
!
! La gram�tica por defecto aqu� propuesta no espera conjunciones. No
! puede por tanto parsear algo como COGE CAJA Y CUBO, ni SALTA Y
! BAILA. Es funci�n de un parsing previo separar la frase en
! sub-frases y enviar cada una de ellas a este parser.
!
#ifndef _PAWS_H_;
Message "[Incluyendo parser paws-like]";
System_file;
Constant _PAWS_H_;
    
Constant TAMANYO_BUFFER_PARSER 6; ! Seis elementos: verbo, nombre1,
! nombre2, adjetivo1, adjetivo2, preposicion

! Bits que identifican el tipo de palabra (almacenados en el campo
! flags de cada palabra de diccionario)
Constant BIT_ES_VERBO 1;
Constant BIT_ES_PREPOSICION 8;
Constant BIT_ES_CONJUNCION 16;
Constant BIT_ES_ADJETIVO 32;
Constant BIT_ES_NOMBRE 128;


Property additive  sinonimos;
Class 	TVerbo
 with 	numero 0;
Class 	TNombre
 with 	numero 0;
Class 	TAdjetivo
 with 	numero 0;
Class 	TPreposicion
 with 	numero 0;
Class 	TConjuncion
 with 	numero 0;

Object 	Ninguno;  ! Para representar cuando no hay la palabra esperada
Object 	Desconocido ! Para representar un verbo desconocido
 with 	numero -1;

 
TNombre	Pronombre
 with 	sinonimos '-lo' '-la' '-le' '-los' '-las' '-les' '�l' 'ella'
	    'ellos' 'ellas';

! Grafo gramatical que reconoce lenguajes del tipo
! [Verbo] Nombre1 [Adjetivo1] [preposicion] [nombre2] [adjetivo2]
!
! No admite adjetivos delante del nombre, ni un verbo despues de un
! nombre, si bien la mayoria de los componentes pueden no aparecer
! realmente
!
Object 	GramaticaEstandar;
Object -> NodoInicial
 with	accion[ x res;
	    ! Inicializar la tabla de resultados
	    for (x=0:x<TAMANYO_BUFFER_PARSER:x++)
		res-->x=Ninguno;
	],
	siguiente EsVerbo NodoVerbo ! Se espera un verbo en primer lugar
	    EsNombre NodoNombre1;   ! Pero se admite tambien un nombre
Object -> NodoVerbo
 with	accion [ x res;
	    res-->0=x;
	],
	siguiente EsNombre NodoNombre1  ! S�lo se admite nombre o prep
                  EsPreposicion NodoPrepVerbo;   ! tras verbo
Object -> NodoPrepVerbo ! Si aparece una preposici�n pegada al verbo
 with    accion [ x res;
             res-->3=x;
         ],
         siguiente EsNombre NodoNombre1;
Object  -> NodoNombre1
 with	accion [ x res;
	    res-->1=x;
	],                                 ! Tras el nombre puede ir
	siguiente EsAdjetivo NodoAdjetivo1 !   un adjetivo
	    EsPreposicion NodoPreposicion  !   una preposicion
	    EsNombre NodoNombre2           !   u otro nombre
	    ;
Object -> NodoAdjetivo1
 with	accion [x res;
	    res-->2=x;
	],                                      ! Tras el adjetivo solo
	siguiente EsPreposicion NodoPreposicion !   una preposicion
	    EsNombre NodoNombre2 ;              !   o el nombre2
Object -> NodoNombre2
 with	accion [x res;
	    res-->4=x;
	],                                    ! Tras el nombre2
	siguiente EsAdjetivo NodoAdjetivo2;   !  s�lo un adjetivo2  
Object -> NodoPreposicion
 with	accion [x res;
	    if (res-->3==Ninguno)
	      res-->3=x;
	],
	siguiente EsNombre NodoNombre2;
Object -> NodoAdjetivo2
 with	accion [x res;
	    res-->5=x;
	],
	siguiente 0;       ! Tras el adjetivo2 se ignora todo lo dem�s.


Object ParserPaws
 with 	parser [ palabras gram result  ignoradas
 ! Parametro: palabras  (array tokenizado)
 !            gramatica (objeto gramatical raiz)
 ! salidas: result-->0 verbo, result-->1 nombre1, result-->2 nombre2
 !     result-->3 adjetivo1 result-->4 adjetivo2 result-->5 preposicion
 !     ignoradas-->0 n�mero de palabras desconocidas o ignoradas
 !         (ignoradas--1 ... indices en palabras de ellas)
 ! Variables locales:
 x
 palab    ! Palabra en curso
 pos      ! posici�n actual
 ign      ! N�mero de palabras ignoradas
 nodo     ! Estadio del parsing
 ok i      ! auxiliares
 condicion; ! a cumplirse en esta fase del parsing
	    
	    
	    ! Comienza el parsing, desde la primera palabra del buffer
	    pos=0;
	    nodo=child(gram);
	    nodo.accion(x, result);
	    while (1) 
	    {
		if (pos==palabras-->0) break; ! Agotado el buffer
		palab=palabras-->(1+pos++);
		! Determinar si la palabra es alguno de los tokens
		! esperados, y en ese caso, cambiar el nodo gramatical
		! y efectuar la acci�n indicada en �l.

		ok=false;
		! Si no se esperan m�s tokens, se ignoran todos
		if (nodo.siguiente == 0) jump ignorar;
		for (i=0:i<nodo.#siguiente/WORDSIZE:i=i+2)
		{
		    condicion=nodo.&siguiente-->i;
		    x=condicion(palab);
		    if (x) 
		    {
			! Encajada, pasamos al nodo siguiente
			ok=true;
			nodo = nodo.&siguiente-->(i+1);
			nodo.accion(x,result);
!			print "OK ", (object) nodo, "^";
			break;
		    }
		}
		! Si no ha encajado con ninguna de las esperadas, se
		! ignora.
		
		.ignorar;
		if (ok==false)
		{
		    if (ignoradas && ign<ignoradas-->0)
		    	ignoradas-->(WORDSIZE+ign++) = pos-1;
		}
	    }
	    ! Hemos agotado el buffer. Almacenemos los resultados.
	    ignoradas-->0 = ign;
	    return;
	];


!================================================================
! Funciones de categorizaci�n gramatical
!  Reciben una palabra y retornan el objeto gramatical en cuesti�n
!  si es que son del tipo esperado, o 0 si no lo son
!
[ EsVerbo v i;
    if (v->0 ~= $60) rfalse;
    if (v->#dict_par1&BIT_ES_VERBO)
    {
	objectloop(i ofclass TVerbo)
	    if (i.numero == v->(DICT_WORD_SIZE+3) * 256 + v->(DICT_WORD_SIZE+4))
		return i;
    }
    rfalse;
];

[ EsNombre n i;
    if (n->0 ~= $60) rfalse;
    if (n->#dict_par1&BIT_ES_NOMBRE)
    {
	objectloop(i ofclass TNombre)
	    if (i.numero == n->(DICT_WORD_SIZE+3) * 256 + n->(DICT_WORD_SIZE+4))
		return i;
    }
    rfalse;
 ];

[ EsAdjetivo a i;
    if (a->0 ~= $60) rfalse;
    if (a->#dict_par1&BIT_ES_ADJETIVO)
	objectloop(i ofclass TAdjetivo)
	    if (i.numero == a->(DICT_WORD_SIZE+3) * 256 + a->(DICT_WORD_SIZE+4))
    		return i;
    
    rfalse;
];

[ EsPreposicion p i;
    if (p->0 ~= $60) rfalse;
    if (p->#dict_par1&BIT_ES_PREPOSICION)
	objectloop(i ofclass TPreposicion)
	    if (i.numero == p->(DICT_WORD_SIZE+3) * 256 + p->(DICT_WORD_SIZE+4))
		return i;
    rfalse;
];

[ EsConjuncion c i;
    if (c->0 ~= $60) rfalse;
    if (c->#dict_par1&BIT_ES_CONJUNCION)
	objectloop(i ofclass TConjuncion)
	    if (i.numero == c->(DICT_WORD_SIZE+3) * 256 + c->(DICT_WORD_SIZE+4))
		return i;
    rfalse;
];    

! Retorna true si la palabra p no est� en el diccionario
[ EsDesconocida p;
    return (p==0);
];

! Retorna false siempre. Puede ser �til para construir gram�ticas
[ EsIgnorado x;
    x=x;
    rfalse;
];

!================================================================
! Inicializaci�n del m�dulo
!
! Consiste en inicializar el diccionario de la m�quina Glulx,
! a�adiendo a cada palabra ciertos flags que indican si es verbo,
! nombre, etc... y asoci�ndole adem�s un identificador �nico.
! Estos campos de flags y el identificador son usados por las
! rutinas de categorizaci�n gramatical anteriores.
!
! AVISO: Es una rutina de muy bajo nivel que accede directamente a la
! estructura que usa la m�quina Glulx para almacenar el diccionario.
_Gancho_ InicializarVocabulario InicializarModulos
  with
    sig_id 1,
    rutina [i j k p npal tam_entrada;

      npal = #dictionary_table-->0;
      tam_entrada = DICT_WORD_SIZE + 7;
      for (j = 0: j < npal: j++) {
	k = #dictionary_table + WORDSIZE + tam_entrada*j;
	k->#dict_par1 = 0;  ! Se borran todos los flags. La
			    ! palabra no tiene categor�a gramatical
	k->#dict_par2 = 0;
	k->#dict_par3 = 0; ! Se borran tambi�n sus n�meros
      }

      npal = sig_id;  ! Identificador �nico de palabra autoincrementado
      objectloop (i)
      {
        ! Inicializamos las palabras de vocabulario definidas como tal
	if (i ofclass TVerbo or TNombre or TPreposicion or TAdjetivo or
	    TConjuncion)
	{
	  if (i.numero == 0)
	    i.numero = npal++;
	  for (j = 0: j < (i.#sinonimos / WORDSIZE): j++)
	  {
	    p = i.&sinonimos-->j;
	    if (p->(DICT_WORD_SIZE + 3) + p->(DICT_WORD_SIZE + 4) ~= 0)
	      print "^[** AVISO: la palabra '", (address) p, "' ya estaba
		definida en el vocabulario, y se ha redefinido en ",
		(name) i, " **]^";
	    p->(DICT_WORD_SIZE + 3) = i.numero / 256;
	    p->(DICT_WORD_SIZE + 4) = i.numero % 256;
	    if (i ofclass TVerbo)
	      p->#dict_par1 = p->#dict_par1 | BIT_ES_VERBO;
	    if (i ofclass TNombre)
	      p->#dict_par1 = p->#dict_par1 | BIT_ES_NOMBRE;
	    if (i ofclass TPreposicion)
	      p->#dict_par1 = p->#dict_par1 | BIT_ES_PREPOSICION;
	    if (i ofclass TAdjetivo)
	      p->#dict_par1 = p->#dict_par1 | BIT_ES_ADJETIVO;
	    if (i ofclass TConjuncion)
	      p->#dict_par1 = p->#dict_par1 | BIT_ES_CONJUNCION;
	  }
	}
      }
      self.sig_id = npal;
    ];

#ifdef _DEBUG_H_;
! Rutinas de volcado de informaci�n �tiles para depuraci�n
[ MostrarDiccionario j npal tam_entrada wd;
    npal = #dictionary_table-->0;
    tam_entrada = DICT_WORD_SIZE + 7;
    for (j=0:j<npal:j++) {
	wd = #dictionary_table + WORDSIZE + tam_entrada*j;
	print (address) wd, " [";
	MostrarTipoPalabra(wd);
	print "] ";
    }
    print "^";
];

[ MostrarTipoPalabra w ;
    if (w->#dict_par1&BIT_ES_VERBO) print "V";
    if (w->#dict_par1&BIT_ES_NOMBRE) print "N";
    if (w->#dict_par1&BIT_ES_ADJETIVO) print "A";
    if (w->#dict_par1&BIT_ES_PREPOSICION) print "P";
];    

[ MostrarVocabulario ;
    print "Vocabulario del juego.^";
    glk($86, 1);
    print "^Verbos:^";
    glk($86, 0);
    MostrarVocabularioTipo(TVerbo);
    glk($86, 1);
    print "^Sustantivos:^";
    glk($86, 0);
    MostrarVocabularioTipo(TNombre);
    glk($86, 1);
    print "^Adjetivos:^";
    glk($86, 0);
    MostrarVocabularioTipo(TAdjetivo);
    glk($86, 1);
    print "^Preposiciones:^";
    glk($86, 0);
    MostrarVocabularioTipo(TPreposicion);
];

[ MostrarVocabularioTipo tipo i j;
    objectloop(i ofclass tipo)
    {
	print (object) i, " [";
	for (j=0:j<i.#sinonimos/WORDSIZE-1:j++)
	{
	    if (j>0) print ",";
	    print (address) i.&sinonimos-->j;
	}
	print "]^";
    }
];
#endif;
#endif;
