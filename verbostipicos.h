! attoPAWS
! ================================================================
! v0.2.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!

! Verbos tipicos de todos los juegos, para incluir en attoPAWS
! Tambi�n se da una tabla de respuestas por defecto para que
! maneje estos verbos (en respuestas.h).
!
System_file;

! De direcci�n
Tverbo NORTE   class TNombre with sinonimos 'norte' 'n//' has especial;
Tverbo SUR     class TNombre with sinonimos 'sur'   's//' has especial;
Tverbo ESTE    class TNombre with sinonimos 'este'  'e//' has especial;
Tverbo OESTE   class TNombre with sinonimos 'oeste' 'o//' 'w//' has especial;
Tverbo ARRIBA  class TNombre with sinonimos 'arriba' 'u//' 'sube' 'subir'
    has especial;
Tverbo ABAJO   class TNombre with sinonimos 'abajo' 'b//' 'baja' 'bajar'
    has especial;
Tverbo ENTRAR  class TNombre with sinonimos 'entra' 'entrar' 'dentro' 'adentro'
    has especial;
Tverbo SALIR   class TNombre with sinonimos 'sal' 'salir' 'fuera' 'afuera'
    has especial;

! Se debe indicar cu�les son los verbos de direcci�n. Esto lo hacemos
! en la inicializaci�n de este m�dulo.
_Gancho_ InicializarVerbosDireccion InicializarModulos
    with rutina [;
	     SonVerbosDeDireccion(NORTE, SUR, ESTE, OESTE, ARRIBA,
				  ABAJO, ENTRAR, SALIR);
	 ];

! ------------ Habituales
TVerbo EXAMINAR with sinonimos 
               'x//' 'ex' 'examina' 'examinar' 'lee' 'leer' 'leo' 'examino';
Tverbo COGER   with sinonimos 'coge' 'coger' 'cojo';
Tverbo DEJAR   with sinonimos 'deja' 'dejar' 'dejo';
Tverbo INVENTARIO class TNombre with sinonimos 'i//' 'inventario';
TVerbo IR      with sinonimos 
               'ir' 've' 'vete' 'voy' 'anda' 'camina' 'corre';
Tverbo METER   with sinonimos 'mete' 'meter' 'meto';
TVerbo SACAR   with sinonimos 'saca' 'sacar';
Tverbo PONER   with sinonimos 
               'pon' 'poner' 'ponte' 'ponerte' 'ponerse' 'ponerme'
    'pongo' 'ponse';
Tverbo QUITAR  with sinonimos 'quita' 'quitar' 'quitarte' 'quitarse'
               'quitarme' 'quito' 'quitate';
TVerbo MIRAR   with sinonimos 'm//' 'mira' 'mirar' 'miro' 'l//'
               'describir' 'ver';
TVerbo ABRIR   with sinonimos 'abre' 'abrir' 'abro';
TVerbo CERRAR  with sinonimos 'cierra' 'cerrar' 'cierro';
TVerbo MATAR   with sinonimos 
               'mata' 'matar' 'mato' 'ataca' 'atacar' 'ataco' 'pega' 
               'pegar' 'pego' 'golpea' 'golpear' 'golpeo';
TVerbo DECIR   with sinonimos  'di' 'dile' 'decir' 'digo';
TVerbo GRABAR  with sinonimos 
               'save' 'salvar' 'grabar' 'graba' 'grabo';
TVerbo CARGAR  with sinonimos 'load' 'carga' 'cargar' 'restaurar';
TVerbo REINICIAR with sinonimos 'reiniciar' 'reinicia' 'reset';
TVerbo FIN     with sinonimos 
               'fin' 'quit' 'q//' 'acabar' 'acaba' 'acabo' 'terminar';
TVerbo ESPERAR with sinonimos 'espera' 'esperar' 'z//';
TVerbo AYUDA   with sinonimos 'ayuda';

! Se definen tambi�n las conjunciones est�ndar
TConjuncion with sinonimos 'y//' 'luego' 'despues' './/' ',//';

! Algunas preposiciones habituales:
TPreposicion EN with sinonimos 'en' 'dentro';
TPreposicion CON with sinonimos 'con' 'usando';

! Y un par de nombres necesarios para la tabla de respuestas por
! defecto:
TNombre TODO with sinonimos 'todo' 'todos' 'todas'
    has especial; ! No actualiza los pronombres
!________________________________________________________________

