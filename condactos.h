! attoPAWS
! ================================================================
! v0.2.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!

#ifndef _CONDACTOS_H_;
Message "[PseudoCondactos PAWS]";
System_file;
Constant _CONDACTOS_H_;
Include "mainpaws";

!================================================================
! Implementaci�n de los condactos PAWS
!================================================================

!________________________________________________________________
! Condiciones
!................................................................
[ AT loc;
    return localizacion==loc;
];
!................................................................
[ NOTAT loc;
    return localizacion~=loc;
];

! ACGT loc, ATLT loc: No tienen sentido ya que las
! localidades no tienen asignado un n�mero creciente �o tal vez s�? En
! todo caso el tipo de aplicaci�n que pudiera tener este condacto
! entiendo que es para tener "regiones" en el juego, y esto
! puede llevarse a cabo definiendo clases de localidades p.ej:
!
! Class Exteriores class Localidad;
! Class DentroCastillo class Localidad;
! Class Jardines class Localidad;
!
! Y despu�s hacer que las localidades del juego sean de uno de esos
! tipos, con lo que puede saberse en qu� regi�n del juego se halla el
! jugador mediante if (localizacion ofclass Exteriores) ...
!
!................................................................
[ PRESENT x;
    return (WORN(x)||CARRIED(x)||ISAT(x,localizacion));
];
!................................................................
[ ABSENT x;
    return ~~PRESENT(x);
];
!................................................................
[ WORN x;
    return ISAT(x,Puestos);
];
!................................................................
[ NOTWORN x;
    return ~~WORN(x);
];
!................................................................
[ CARRIED x;
    return ISAT(x, jugador);
];
!................................................................
[ NOTCARRIED x;
    return ~~CARRIED(x);
];
!................................................................
[ ISAT x l;
    l=LocnoPlus(l);
    while ((x=parent(x))~=0)
    {
	if ((l has recipiente)&&(l hasnt abierto)) rfalse;
	if (x==l) rtrue;
    }
    rfalse;
];
!................................................................
[ ISNOTAT x l;
    return ~~ISAT(x,l);
];
!................................................................
! ZERO, NOTZERO, EQ, NOTEQ, GT, LT, SAME, NOTSAME
! no han sido implementadas. Pueden usarse en su lugar los operadores
! de comparaci�n de inform:
!
! ZERO x        ---> x==0
! NOTZERO x     ---> x~=0
! EQ x valor    ---> x==valor
! NOTEQ x valor ---> x~=valor
! GT x valor    ---> x>valor
! LT x valor    ---> x<valor
! SAME x y      ---> x==y
! NOTSAME x y   ---> x~=y
!................................................................
! An�logamente tampoco existen ADJECT1, ADJECT2, PREP, NOUN2
! pudiendo usarse en su lugar:
!
!  if (ADJ1==PALABRA)
!  if (ADJ2==PALABRA)
!  if (PREP=PALABRA)
!  if (NOMBRE2==PALABRA)
!................................................................
[ CHANCE percent;
    return (random(100)>=percent);
];
!................................................................
[ TIMEOUT;
    return HuboTimeout_;
];
!................................................................
[ QUIT x y;
    SYSMESS(12);
    ! Averiguamos la primera letra del sysmess 30
    ImprimirEnMemoria(gg_arguments, 1, SYSMESS, 30);
    x=glk($A0, gg_arguments->0); ! Pasarla a min�sculas
    BN_Input(gg_main, auxiliar);
    y = glk($A0, auxiliar->4); ! Pasarla a min�sculas
    if (x==y) rtrue;
    NEWTEXT();
];

!________________________________________________________________
! Acciones
!................................................................
[ GET x;
    Objeto1=x;
    if (WORN(x)||CARRIED(x)) ErrorAbortar(25); ! Ya tienes _
    if (ABSENT(x)) ErrorAbortar(26); ! No veo eso
    if (PesoLlevado(jugador)+PesoLlevado(x)+
	PesoLlevado(puestos)>jugador.max_peso)
	ErrorAbortar(43); ! Pesa demasiado
    if (children(jugador)>=jugador.capacidad) ErrorAbortar(27);
    move x to jugador;
    SYSMESS(36); ! Cogido.
];
!................................................................
[ DROP x;
    Objeto1=x;
    if (WORN(x)) ErrorAbortar(24); ! Lo llevas puesto
    if (ISAT(x,localizacion)&&NOTCARRIED(x)&&NOTWORN(x))
	ErrorAbortar(49); ! No tienes _
    if (ISNOTAT(x,localizacion)&&NOTCARRIED(x)&&NOTWORN(x))
	ErrorAbortar(28); ! No tienes eso
    move x to localizacion;
    SYSMESS(39); ! Dejado.
];
!................................................................
[ WEAR x;
    Objeto1=x;
    if (ISAT(x,localizacion)&&NOTCARRIED(x)&&NOTWORN(x))
	ErrorAbortar(49); ! No tienes _
    if (WORN(x)) ErrorAbortar(29); ! Ya lo llevas puesto
    if (NOTCARRIED(x)) ErrorAbortar(28);  ! No tienes eso
    if (x hasnt prenda) ErrorAbortar(40); ! No puedes ponerte _
    move x to puestos;
    SYSMESS(37); ! Te lo pones
];
!................................................................
[ REMOVE x;
    Objeto1=x;
    if (ISAT(x,localizacion)&&NOTWORN(x))
	ErrorAbortar(50); ! No llevas puesto _
    if (NOTWORN(x)) ErrorAbortar(23);  ! No llevas puesto eso
    if (x hasnt prenda) ErrorAbortar(41); ! No te puedes quitar _
    if (children(jugador)==jugador.capacidad)
	ErrorAbortar(42);  !Tienes las manos llenas
    move x to jugador;
    SYSMESS(38); ! Te quitas _
];
!................................................................
! CREATE es palabra reservada Inform. En su lugar llamamos a este
! condacto PCREATE
[ PCREATE x;
    move x to localizacion;
];
!................................................................
! DESTROY es palabra reservada Inform. En su lugar llamamos a este
! condacto PDESTROY
[ PDESTROY x;
    move x to limbo;
];
!................................................................
[ SWAP x y px py;
    px=parent(x);
    py=parent(y);
    move x to py;
    move y to px;
];
!................................................................
[ PLACE x l;
    l=LocnoPlus(l);
    move x to l;
];
!................................................................
[ PUTO l;
    l=LocnoPlus(l);
    move Objeto1 to l;
];
!................................................................
[ PUTIN x y;
    if (y hasnt recipiente) "^[** Error PAWS: se ha intentado PUTIN(",
	(name)x , ", ", (name)y , ") pero ", (name) y, " no es un
	recipiente **]";
    if (WORN(x)) ErrorAbortar(24);  ! Lo llevas puesto
    if (ISAT(x,localizacion)&&NOTWORN(x)&&NOTCARRIED(x))
	ErrorAbortar(49); ! No tengo _
    if (ISNOTAT(x, localizacion)&&NOTWORN(x)&&NOTCARRIED(x))
	ErrorAbortar(28); ! no tengo eso
    move x to y;
    Objeto1=x;
    Objeto2=y;
    SYSMESS(44);  !Metes tal en cual
];
!................................................................
[ TAKEOUT x y;
    Objeto1=x;
    Objeto2=y;
    if (y hasnt recipiente)
	"^[** Error PAWS: se ha intentado TAKEOUT(", (name) x, ", ",
	    (name) y, ") pero ", (name) y , " no es un recipiente
	    **]";
    if (WORN(x)||(x in jugador)) ErrorAbortar(25); ! Ya tienes...
    if (ISNOTAT(x,y)&&ISAT(x, localizacion))
	ErrorAbortar(45);  ! El _ no est� en el _
    if (ISNOTAT(x, localizacion)&&ISNOTAT(x,y))
    	ErrorAbortar(52);  ! Eso no est� en el _
    if (NOTCARRIED(y)&&NOTWORN(y))
    {
    	if (PesoLlevado(jugador)+PesoLlevado(x)+
	    PesoLlevado(puestos)>jugador.max_peso)
	    ErrorAbortar(43); ! Pesa demasiado
    }
    if (children(jugador)>=jugador.capacidad)
	ErrorAbortar(27);
    move x to jugador;
    SYSMESS(36);  ! Cogido
];
!................................................................
! DROPALL no implementado

! Haciendo uso de la funci�n MetaAUTO es f�cil implementar los
! condactos AUTO* de paws:
!................................................................
[ AUTOG;
    Objeto1=MetaAuto(Nombre1, Adj1, localizacion, jugador, puestos);
    if (Objeto1==Ninguno) ErrorAbortar(26); ! No veo eso
    if (Objeto1 == 0 or jugador)
        ErrorAbortar (8);  ! No puedo hacer eso
    GET(Objeto1);
];
!................................................................
[ AUTOD;
    Objeto1=MetaAuto(Nombre1, Adj1, jugador, puestos,localizacion);
    if (Objeto1==Ninguno) ErrorAbortar(26); ! No veo eso
    if (Objeto1 == 0 or jugador)
        ErrorAbortar (8);  ! No puedo hacer eso
    DROP(Objeto1);
];
!................................................................
[ AUTOW;
    Objeto1=MetaAuto(Nombre1, Adj1, jugador, puestos, localizacion);
    if (Objeto1==Ninguno) ErrorAbortar(26); ! No veo eso
    if (Objeto1 == 0 or jugador)
        ErrorAbortar (8);  ! No puedo hacer eso
    WEAR(Objeto1);
];
!................................................................
[ AUTOR;
    Objeto1=MetaAuto(Nombre1, Adj1, puestos, jugador, localizacion);
    if (Objeto1==Ninguno) ErrorAbortar(26); ! No veo eso
    if (Objeto1 == 0 or jugador)
        ErrorAbortar (8);  ! No puedo hacer eso
    REMOVE(Objeto1);
];
!................................................................
[ AUTOP donde;
    Objeto1=MetaAuto(Nombre1, Adj1, jugador, puestos, localizacion);
    if (Objeto1==Ninguno) ErrorAbortar(26); ! No veo eso
    if (Objeto1 == 0 or jugador)
        ErrorAbortar(8);  ! No puedo hacer eso
    PUTIN(Objeto1, donde);
];
!................................................................
[ AUTOT donde;
    Objeto1=MetaAuto(Nombre1, Adj1, donde, jugador, puestos,
		      localizacion);
    if (Objeto1==Ninguno) ErrorAbortar(26); ! No veo eso
    if (Objeto1 == 0 or jugador)
        ErrorAbortar(8);  ! No puedo hacer eso
    TAKEOUT(Objeto1, donde);
];

!................................................................
[ COPYOO obj1 obj2;
    move obj2 to parent(obj1);
    Objeto1=obj2;
];
!................................................................
! COPYOF No implementado. Para averiguar d�nde est� un objeto puede
! usarse parent(objeto)
!................................................................
! COPYFO No implementado. Para mover un objeto puede usarse
!   move objeto to lugar;
!................................................................
[ WHATO;
    Objeto1=MetaAuto(Nombre1, Adj1, jugador, puestos, localizacion);
    if (Objeto1==Ninguno or 0 or -1) rfalse;
    return Objeto1;
];
!................................................................
! Esta no exist�a en PAWS, pero es �til
[ WHATO2;
    Objeto2=MetaAuto(Nombre2, Adj2, jugador, puestos, localizacion);
    if (Objeto2==Ninguno or 0 or -1) rfalse;
    return Objeto2;
];

!................................................................
[ WEIGH o;
    return PesoLlevado(o);
];
!................................................................
! SET, CLEAR, LET, PLUS, MINUS, ADD, SUB, COPYFF
! no implementadas. Usar los operadores Inform en su lugar
!
!................................................................
! MOVE est� implementado de forma ligeramente diferente. En el PAWS
! original serv�a para detectar si un movimiento era posible en una
! localidad dada (usando VERBO para averiguar en qu� direcci�n
! moverse), y en caso de que fuera posible, cambiaba el valor del flag
! para que indicara la nueva localidad.
!
! En esta implementaci�n funciona igual, s�lo que en lugar de cambiar
! un flag, retorna el valor de la nueva localidad.
[ MOVE desde  destino;
    ! desde=localidad desde la que se pretende mover
    ! retorna=localidad a la que se llega o false si no se
    ! puede mover en la direcci�n solicitada por VERBO
    destino = VERBO;
    while (destino ofclass TVerbo)
    {
	destino = IntentarIr (destino, desde);

	if (metaclass (destino) == Routine)
	    destino = destino();
    }
    if (destino ofclass Localidad)
	return destino;

    rfalse;  ! No se puede por all�
];

! Este condacto es a�adido, sirve para averiguar si un verbo dado es
! un verbo de direcci�n
[ ISVDIR v;
    if (v==0) v=verbo;
    return (v has especial);
];

[ GOTO loc;
    move jugador to loc;
    localizacion=loc;
];

[ WEIGHT;
    return PesoLlevado(jugador)+PesoLlevado(puestos)-jugador.peso;
];

[ ABILITY objs peso;
    jugador.max_peso = peso;
    jugador.capacidad= objs;
];

[ PROMPT sysno;
    PromptActual=sysno;
];

[ TIME dur opt;
    Temporizacion_=(dur*1280)/1000;
    opt=opt;
];

[ INPUT opt;
    opt=opt; ! No implementado
];

[ TURNS;
    SYSMESS(17);
    print turnos_;
    SYSMESS(18);
    if (turnos_~=1) SYSMESS(19);
    SYSMESS(20);
];

[ SCORE;
    SYSMESS(21);
    print puntuacion_;
    SYSMESS(22);
];

[ CLS;
    LimpiarVentana(gg_main);
];

[ NEWLINE;
    glk($86,0);
    new_line;
];

[ LISTOBJ;
    if (children(localizacion)>1)
    {   new_line;
	SYSMESS(1);
	ListarContenidos(localizacion);
	SYSMESS(48);
    }
];
[ LISTAT obj;
    obj=LocnoPlus(obj);
    if (children(obj)==0) SYSMESS(53);
    else ListarContenidos(obj);
];

[ INVEN i;
    SYSMESS(9);
    if (children(jugador)+children(puestos)==0) SYSMESS(11);
    objectloop(i)
    {
	if ((i in jugador)||(i in puestos)) {
	    print (un) i;
	    if (i in puestos) SYSMESS(10);
	    print "^";
	}
    }
    DONE();
];

[ SAVE res fref;
    fref = glk($0062, $01, $01, 0); ! fileref_create_by_prompt
    if (fref == 0)
    	jump SFailed;
    handle_save = glk($0042, fref, $01, 301); ! stream_open_file
    glk($0063, fref); ! fileref_destroy
    if (handle_save == 0) {
      	jump SFailed;
    }
#Ifdef _FILTROS_H_;
    DesactivarFiltroSalida();
#Endif;  ! _FILTROS_H_
    @save handle_save res;
#Ifdef _FILTROS_H_;
    ActivarFiltroSalida();
#Endif;  ! _FILTROS_H_
    if (res == -1) {
      	! Si llegamos a este punto es que el juego acaba de ser cargado.
      	EjecutarGancho(ReinicializarGlk);
      	glk($0044, handle_save, 0); ! stream_close
	handle_save=0;
      	DESC();
    }
    ! En este otro punto, acaba de ser salvado
    glk($0044, handle_save, 0); ! stream_close
    handle_save=0;
    if (res == 0) "Partida grabada.";
    .SFailed;
    "Error al grabar la partida.";
];

[ LOAD res fref;
    fref = glk($0062, $01, $02, 0); ! fileref_create_by_prompt
    if (fref == 0)
    	jump RFailed;
    handle_save = glk($0042, fref, $02, 301); ! stream_open_file
    glk($0063, fref); ! fileref_destroy
    if (handle_save == 0) {
    	jump RFailed;
    }
#Ifdef _FILTROS_H_;
    DesactivarFiltroSalida();
#Endif;  ! _FILTROS_H_
    @restore handle_save res;
#Ifdef _FILTROS_H_;
    ActivarFiltroSalida();
#Endif;  ! _FILTROS_H_
    if (res~=1) jump Rfailed;
    glk($0044, handle_save, 0); ! stream_close
.RFailed;
    "Error al recuperar la partida.";
];

[ DOALL lugar tabla
    i j x nom adj nom_orig adj_orig;

    if (doall_ejecutandose_) rfalse;
    if (tabla==0)
    {
	if (indice_pila_procesos_==0)
	    "^[** Error PAWS: DOALL s�lo puede ser llamado desde un
	    proceso o tabla de respuestas, no desde una rutina. **]";
	tabla=pila_procesos_-->(indice_pila_procesos_-1);
    }

    doall_ejecutandose_=true;
    nom_orig=Nombre1;
    adj_orig=Adj1;
    lugar=LocnoPlus(lugar);

#Ifdef DEBUG;
    if (depuracion_paws&BIT_TRAZAR_CONDACTOS)
	print "[DOALL(", (name) lugar, ")]^";
#Endif; ! DEBUG

    objectloop(i)
    {
	nom = 0; adj = 0;
	if (i notin lugar) continue;
	if (i==jugador) continue;
	if (i provides nombre)
	    for (j=0:j<i.#nombre/WORDSIZE:j++)
	    {
		if (i.&nombre-->j ofclass TNombre) nom=i.&nombre-->j;
		if (i.&nombre-->j ofclass TAdjetivo)
		    adj=i.&nombre-->j;
	    }
	if (i provides sinonimos)
	    nom=EsNombre(i.sinonimos);
	if (i provides adjetivo)
	    adj=EsAdjetivo(i.adjetivo);
	if (nom==0) nom=Ninguno;
	if (adj==0) adj=Ninguno;
	Nombre1=nom;
	Adj1=adj;
	if (children(tabla))
	{
	    objectloop(j in tabla)
	    {
		done_=0;
	    	x=EjecutarTablaRespuestas(Verbo, Nombre1, j);
	    	if (x) break;
	    	if (done_==1) {x=true; break;}
	    	if (done_==-1) {
		    SYSMESS(8);  !No puedes hacer eso
		    x=true;
		    break;
	    	}
	    }
	} else	EjecutarTablaRespuestas(verbo, nom, tabla);
    }
    Nombre1=nom_orig;
    Adj1=adj_orig;
    doall_ejecutandose_=false;
    rtrue;
];


[ PARSE j;
    if (IndicePrimeraComilla == 0 || PosicionEnBufferPSI == IndiceUltimaComilla)
	rfalse;
    PosicionEnBufferPSI++;
    while (EsConjuncion(par-->PosicionEnBufferPSI)) PosicionEnBufferPSI++;
    for (j=1:PosicionEnBufferPSI<IndiceUltimaComilla
	 :PosicionEnBufferPSI++)
    {
	if (EsConjuncion(par-->PosicionEnBufferPSI)) {
	    break;
	}
	par2-->(j++)=par-->PosicionEnBufferPSI;
    }
    par2-->0=j-1;
    ignoradas-->0=20;
    parserPAWS.parser(par2, GramaticaActual,
		     result, ignoradas);
    if (ignoradas-->0 >= par2-->0)
    {
     	return -1; ! NO se ha entendido nada
    }
    Verbo=result-->0;
    Nombre1=result-->1;
    Adj1=result-->2;
    Prep=result-->3;
    Nombre2=result-->4;
    Adj2=result-->5;
    ! Marcar si realmente se ha especificado un nombre
    if (par2-->0 < 3)
    {
	Nombre2=__;
	Adj2=__;
	Prep=__;
	Adj1=__;
    }
    if (par2-->0 <2)
	Nombre1=__;
    rtrue;
];


[PROCESS p;
    EjecutarTablaRespuestas(Verbo, Nombre1, p);
];


!================================================================
! SYSMESS y MES Implementa las tablas de mensajes de sistema m�ltiples
[ SYSMESS n x i;
    objectloop(i in TablasDeMensajesDeSistema)
    {
	x=EjecutarTablaMensajes(i, n);
	if (x) break;
    }
];


[ MES n x i;
    if (MetaTipo(n)==TIPO_STRING) {
	print (string) n;
	return;
    }
    objectloop(i in TablasDeMensajesDeUsuario)
    {
	x=EjecutarTablaMensajes(i, n);
	if (x) break;
    }
];


!================================================================
! Comandos que rompen el flujo normal
[ DESC;
    doall_ejecutandose_=false;
    @throw 2 desc_token;
];

[ NEWTEXT;
    doall_ejecutandose_=false;
    @throw 3 desc_token;
];

[ DONE;
    doall_ejecutandose_=false;
    done_=1;
];

[ OK;
    SYSMESS(15);
    DONE();
];

[ NOTDONE;
    doall_ejecutandose_=false;
    done_=-1;
];

!================================================================
! Rutinas de utilidad para hacer los condactos anteriores
[ ListarContenidos obj clase
    i n j;
    if (clase==0) clase=Object;
    objectloop(i in obj)
	if ((i~=jugador)&&(i ofclass clase)) n++;
    if (n==0)
    {
	SYSMESS(53);
	rtrue;
    }
    objectloop(i in obj)
    {
	if ((i~=jugador)&&(i ofclass clase))
	{
	    j++;
	    print (un) i;
	    if (j==n-1) SYSMESS(47);
	    else if (j==n) ;
	    else SYSMESS(46);
	}
    }
];

! La siguiente funci�n resulta c�moda para implementar muchas de las
! acciones de PAWS, que en ecaso de error emiten un mensaje y generan
! NEWTEXT.
[ ErrorAbortar n;
    SYSMESS(n);
    NEWTEXT();
];

! Funci�n que calcula cu�nto peso hay en un objeto dado (incluyendo el
! propio peso de ese objeto). Tambi�n se le puede aplicar al jugador
! para saber cu�nto peso lleva
[ PesoLlevado x p i;
    if (x.peso==0) return 0;
    if (children(x)==0)
	return x.peso;
    objectloop(i in x)
	p=p+PesoLlevado(i);
    return p;
];

! Esta funci�n intenta hacer un matching del nombre y adjetivo que
! recibe como primeros par�metros con el objeto que recibe como tercer
! par�metro. Retorna true si el objeto tiene ese nombre y adjetivos, y
! false si no.
[ MatchNombreAdj nom adj obj;
!    print "Match: N=", (name) Nombre1, " A=", (name) Adj1, " OBJ=",
!	(name) obj, "^";

    if ((nom==obj)&&(adj==obj)) rtrue;
    if ((nom==obj)&&(adj==__ or Ninguno)) rtrue;
    if (BuscarEnPropiedad(nom, obj, nombre))
    {
	if (adj==Ninguno or __) rtrue;
	if (adj==obj) rtrue;
	if (BuscarEnPropiedad(adj, obj, nombre))
	    rtrue;
    }
    rfalse;
];


! La siguiente funci�n trata de determinar autom�ticamente a qu�
! objeto se est�n refiriendo las palabras N y Adj, mirando si
! encajan con alg�n objeto de los presentes en los tres lugares que
! recibe como par�metro (y en ese orden)
! Retorna el objeto encajado, o bien Ninguno si no hay ese nombre en
! el juego, o bien 0 si hay el nombre, pero no lo ha encontrado en las
! listas suministradas
[ MetaAUTO n adj primero segundo tercero cuarto
   i;
    if (n==Ninguno) return n;
    ! Si es de la clase objeto, tal vez se refiera a si mismo
    if (primero) objectloop(i in primero)
	if (MatchNombreAdj(n,adj,i)) return i;

    if (segundo) objectloop(i in segundo)
	if (MatchNombreAdj(n,adj,i)) return i;

    if (tercero) objectloop(i in tercero)
	if (MatchNombreAdj(n,adj,i)) return i;

    if (cuarto) objectloop(i in cuarto)
	if (MatchNombreAdj(n,adj,i)) return i;

    rfalse;
];

[ ANYKEY;
    SYSMESS(16);
    return BN_Tecla(gg_main);
];

! Esta recibe un locno+ (o sea, una localidad o uno de los n�meros
! 252, 253, 254 y 255) y retorna la "verdadera localidad" a la que se
! intenta hacer referencia
[ LocnoPlus l;
    switch(l)
    {
     252: return Limbo;
     253: return Puestos;
     254: return Jugador;
     255: return localizacion;
     default: return l;
    }
];

! �til para usar como condacto, sin tener que embeber una rutina
[ RESTART;
    @restart;
];

[ END x y;
    SYSMESS(13);
    ImprimirEnMemoria(gg_arguments, 1, SYSMESS, 30);
    x=glk($A0, gg_arguments->0); ! Pasarla a minusculas
    BN_Input(gg_main, auxiliar);
    y = glk($A0, auxiliar->4); ! Pasarla a minusculas
    if (x==y) @restart;
    SYSMESS(14);
    quit;
];

! Algun condacto para cambiar el estilo de forma amigable
[ TITULAR s;
    glk($86,3);
    Output(s);
    glk($86,0);
];

! Lista las salidas de la localidad actual.
[ EXITS  i n listar almenosuna;
    SYSMESS (60);  ! Salidas visibles:
    if (localizacion provides salidas)
    {
	n = localizacion.#salidas / WORDSIZE;
	for (i = 0: i < n: i = i + 3)
	{
	    ! Comprobamos si tenemos que listarla o no
	    listar = localizacion.&salidas-->(i + 1);
	    if (metaclass (listar) == Routine)
		listar = listar();
	    if (~~listar)
		continue;  ! No tenemos que listarla

	    if (almenosuna)
		SYSMESS (62);  ! La coma
	    print (vocabulario) localizacion.&salidas-->i;
	    almenosuna = true;
	}
    }
    if (~~almenosuna)
	SYSMESS (61);  ! Ninguna
    SYSMESS (63);  ! El punto final
    rtrue;
];

! Muestra la descripci�n de un objeto
[ DESCRIBE o;
    Objeto1=o;
    if (o provides descripcion)
	Output(o, descripcion);
    else {
	if ((o hasnt recipiente)||(o hasnt abierto)||(children(o)==0))
	    SYSMESS(64);  ! Nada especial
    }
    if ((o has recipiente)&&(o has abierto)&&children(o))
    {
	SYSMESS(65);
	ListarContenidos(o);
	SYSMESS(48);
    }
];

[ AUTODESCRIBE i;
    i=BuscarEnPropiedad(Nombre1, localizacion, decorado);
    if (i)	{
	Output(localizacion.&decorado-->i);
	if (MetaTipo(localizacion.&decorado-->i)==TIPO_STRING)
	    new_line;
	rtrue;
    }
    Objeto1=MetaAuto(Nombre1, Adj1, localizacion, jugador, puestos);
    if (Objeto1==Ninguno) ErrorAbortar(26); ! No veo eso
    if (Objeto1==0) ErrorAbortar(8);  ! No puedo hacer eso
    DESCRIBE(Objeto1);
];

! Lista los contenidos de un objeto, si es que pueden verse
[ LISTCONT o;
    if (o hasnt recipiente) {
	SYSMESS(66); ! No se puede mirar dentro
	return;
    }
    if (o hasnt abierto) {
	SYSMESS(67); ! Est� cerrado
	return;
    }
    if (children(o)==0) {
	SYSMESS(68);  ! No hay nada
	return;
    }
    SYSMESS(65);
    ListarContenidos(o);
    SYSMESS(48);
];

[ AUTOLISTCONT;
    Objeto1=MetaAuto(Nombre1, Adj1, localizacion, jugador, puestos);
    if (Objeto1==Ninguno) ErrorAbortar(26); ! No veo eso
    if (Objeto1==0) ErrorAbortar(8);  ! No puedo hacer eso
    LISTCONT(Objeto1);
];

[ ISLIGHT x;
    if (localizacion has luz) rtrue;
    objectloop(x ofclass Objeto)
	if ((x has luz) && PRESENT(x)) rtrue;
    rfalse;
];

#endif;
